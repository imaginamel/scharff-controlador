# SCHARFF Backend FACEBOOK CONVERSATION

Este componente funciona como integrador entre Watson Conversation y Facebook Workplace

## Run the app locally

1. [Install Node.js][]
1. cd into this project's root directory
1. Run `npm install` to install the app's dependencies
1. Run `npm start` to start the app
1. Access the running app in a browser at <http://localhost:6001>

[Install Node.js]: https://nodejs.org/en/download/
