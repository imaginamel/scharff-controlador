var request = require('request');
var utils = require('./utils');
var moment = require('moment-timezone');
const sql = require('./postgres.js');
const db = new sql();
var Log = require('./models/Log.js');
var User = require('./models/User.js');

module.exports = {

    validarPorGuia: function(context, callback) {
    	var input_guia = context.input_guia;
    	var options = {
		    uri: 'https://scharff-int-core.mybluemix.net/api/scharff/validarguia/' + input_guia,
		    method: 'GET',
		    timeout: 80000
		}
    	request(options, function (error, response, body) {
		  if (error || response.statusCode != 200) {
		  	console.log("Error en Validar por Guia WS 001");
		  	callback(null, context);
		  } else {
		  	try {
		  		var bodyJ = JSON.parse(body);
		  		console.log('body',bodyJ);
				if (bodyJ.error) {
					console.log('ENTRE AL ERROR');
	            } else {
	            	if(bodyJ[0].GUIA_NO_EXISTE == 1){
	            		console.log('ENTRE A GUIA NO EXISTE');
	            		context.guia_existe=false;
	            		context.guia=null,
	            		context.guia_nro=null;
	            		context.guia_anio=null;
	            		context.es_importacion= null;
	            		context.es_exportacion= null;
	            	}else{
	            		console.log('ENTRE A GUIA EXISTE');
	            		context.guia_existe=true;
	            		context.guia=bodyJ[0].GUIA;
	            		context.guia_nro=bodyJ[0].GUIA_NRO;
	            		context.guia_anio=bodyJ[0].GUIA_ANIO;
	            		context.es_importacion= bodyJ[0].ES_IMPORTACION;
	            		context.es_exportacion= bodyJ[0].ES_EXPORTACION;
	            	}
	            }
	            callback(null, context);
		  	} catch(e) {
		  		console.log("Error en Validar por Guia WS 002",e);
		  		callback(null, context);
		  	}
		  }
		});
    },
    validarPorPersona: function(context, callback) {
    	var input_nombre = context.input_nombre;
    	var options = {
		    uri: 'https://scharff-int-core.mybluemix.net/api/scharff/validarpersona/' + input_nombre,
		    method: 'GET',
		    timeout: 80000
		}
    	request(options, function (error, response, body) {
		  if (error || response.statusCode != 200) {
		  	console.log("Error en Validar por Persona WS 001");
		  	callback(null, context);
		  } else {
		  	try {
		  		console.log('bodybody',body);

		  		var bodyJ = JSON.parse(body);
		  		console.log('DIMENSION',bodyJ.length);

		  		console.log('body',bodyJ);

				if (bodyJ.error) {
					console.log('ENTRE AL ERROR');
	            } else {
	            	if(bodyJ.length == 0){
	            		console.log('NO EXISTEN GUIAS CON ESE NOMBRE');
	            		context.guias_persona=null;
	            		context.guias_persona_dim=0;
	            	}else{
	            		console.log('SI EXISTEN GUIAS CON ESE NOMBRE');
	            		context.guias_persona=bodyJ;
	            		context.guias_persona_dim=bodyJ.length;
	            	}
	            }
	            callback(null, context);
		  	} catch(e) {
		  		console.log("Error en Validar por Persona WS 002",e);
		  		callback(null, context);
		  	}
		  }
		});
    },
    validarPorEmpresa: function(context, callback) {
    	var input_nombre = context.input_nombre;
    	var options = {
		    uri: 'https://scharff-int-core.mybluemix.net/api/scharff/validarempresa/' + input_nombre,
		    method: 'GET',
		    timeout: 80000
		}
    	request(options, function (error, response, body) {
		  if (error || response.statusCode != 200) {
		  	console.log("Error en Validar por Empresa WS 001");
		  	callback(null, context);
		  } else {
		  	try {
		  		console.log('bodybody',body);

		  		var bodyJ = JSON.parse(body);
		  		console.log('DIMENSION',bodyJ.length);

				if (bodyJ.error) {
					console.log('ENTRE AL ERROR');
	            } else {
	            	if(bodyJ.length == 0){
	            		console.log('NO EXISTEN GUIAS CON ESE NOMBRE EMPRESA');
	            		context.guias_empresa=null;
	            		context.guias_empresa_dim=0;
	            	}else{
	            		console.log('SI EXISTEN GUIAS CON ESE NOMBRE EMPRESA');
	            		context.guias_empresa=bodyJ;
	            		context.guias_empresa_dim=bodyJ.length;
	            	}
	            }
	            callback(null, context);
		  	} catch(e) {
		  		console.log("Error en Validar por Persona WS 002",e);
		  		callback(null, context);
		  	}
		  }
		});
    },
    enviarAlerta: function(context, callback) {
    	var input_nombre = context.input_nombre;
    	var options = {
		    uri: 'https://gmail-conv-core.mybluemix.net/api/gmail/alerta',
		    form: {
		    	"conversation_id":context.conversation_id,
		    	"nombre": context.nombre_cliente,
		    	"telefono": context.telefono_cliente,
		    	"email": context.email_cliente,
		    	"intent": context.subjet_alerta,
		    	"tipo_alerta": context.tipo_alerta,
		    	"guia_referencia": context.guia_referencia,
		    	"reclamo_texto": context.reclamo_texto
		    },
		    method: 'POST',
		    timeout: 80000
		}
    	request(options, function (error, response, body) {
		  if (error || response.statusCode != 200) {
		  	console.log("Error en Alerta GMAIL WS 001");
		  	context.exito=null;
		  	callback(null, context);
		  } else {
		  	try {
		  		var bodyJ = JSON.parse(body);
				if (bodyJ.error) {
					console.log('Error en Alerta GMAIL WS 003');
					context.exito=null;
	            } else {
	            	if(bodyJ.exito){
	            		context.exito = true;
	            	}else{
	            		context.exito=null;
	            	}
	            }
	            callback(null, context);
		  	} catch(e) {
		  		console.log("Error en ALERTA GMAIL WS 002",e);
		  		callback(null, context);
		  	}
		  }
		});
    },
    estadoFedex: function(context, callback) {
    	var guia = context.guia;
    	var numero = context.guia_nro;
    	var anio = context.guia_anio;

    	var options = {
		    uri: 'https://scharff-int-core.mybluemix.net/api/scharff/estadoFedex?anio=' + anio +'&numero='+numero+'&guia='+guia,
		    method: 'GET',
		    timeout: 80000
		}
    	request(options, function (error, response, body) {
		  if (error || response.statusCode != 200) {
		  	console.log("Error en estadoFedex WS 001");
		  	callback(null, context);
		  } else {
		  	try {
		  		var bodyJ = JSON.parse(body);
		  		console.log('body',bodyJ);
				if (bodyJ.error) {
					console.log('ERROR EN WEBSERVICE MANUAL');
					context.fedex_estado_cod=null;
            		context.fedex_estado_des=null;
            		context.fedex_estado_obs=null;
	            } else {
	            	if(bodyJ[0].ESTADO_COD && bodyJ[0].ESTADO_COD != ''){
	            		console.log('TIENE ESTATUS FEDEX');
	            		context.fedex_estado_cod=bodyJ[0].ESTADO_COD;
	            		context.fedex_estado_des=bodyJ[0].ESTADO_DES;
	            		context.fedex_estado_obs=bodyJ[0].ESTADO_OBS;
	            	}else{
	            		console.log('NO TIENE ESTATUS FEDEX');
	            		context.fedex_estado_cod=null;
	            		context.fedex_estado_des=null;
	            		context.fedex_estado_obs=null;
	            	}
	            }
	            callback(null, context);
		  	} catch(e) {
		  		console.log("Error en estadoFedex WS 002",e);
		  		callback(null, context);
		  	}
		  }
		});
    },
    estadoSiccsa: function(context, callback) {
    	var guia = context.guia;
    	var numero = context.guia_nro;
    	var anio = context.guia_anio;

    	var options = {
		    uri: 'https://scharff-int-core.mybluemix.net/api/scharff/estadosiccsa?anio=' + anio +'&numero='+numero+'&guia='+guia,
		    method: 'GET',
		    timeout: 80000
		}
    	request(options, function (error, response, body) {
		  if (error || response.statusCode != 200) {
		  	console.log("Error en estadoSiccsa WS 001");
		  	callback(null, context);
		  } else {
		  	try {
		  		var bodyJ = JSON.parse(body);
		  		console.log('body',bodyJ);
				if (bodyJ.error) {
					console.log('ERROR EN WEBSERVICE MANUAL');
	            } else {
	            	if(bodyJ[0].ESTADO_COD && bodyJ[0].ESTADO_COD != ''){
	            		console.log('TIENE ESTATUS SICCSA');
	            		context.siccsa_estado_cod=bodyJ[0].ESTADO_COD;
	            		context.siccsa_estado_des=bodyJ[0].ESTADO_DES;
	            		context.siccsa_estado_obs=bodyJ[0].ESTADO_OBS;
	            		context.siccsa_fecha_compromiso=bodyJ[0].FECHA_COMPROMISO;
	            		context.siccsa_quien_paga=bodyJ[0].QUIEN_PAGA;
	            		context.siccsa_tiene_volante=bodyJ[0].TIENE_VOLANTE;
	            		context.siccsa_tiene_declaracion_aduanera=bodyJ[0].TIENE_DECLARACION_ADUANERA;
	            		context.siccsa_tiene_numeracion=bodyJ[0].TIENE_NUMERACION;
	            		context.siccsa_es_sli=bodyJ[0].ES_SLI;
	            		context.siccsa_age_adu_scharff=bodyJ[0].AGE_ADU_SCHARFF;
	            		context.siccsa_se_entrego_docs=bodyJ[0].SE_ENTREGO_DOCS;
	            		context.siccsa_derechos_monto=bodyJ[0].DERECHOS_MONTO;
	            		context.siccsa_derechos_moneda=bodyJ[0].DERECHOS_MONEDA;
	            		context.siccsa_gastos_monto=bodyJ[0].GASTOS_MONTO;
	            		context.siccsa_gastos_moneda=bodyJ[0].GASTOS_MONEDA;
	            		context.siccsa_cambio_regimen=bodyJ[0].ES_CAMBIO_DE_REGIMEN;
	            	}else{
	            		console.log('NO TIENE ESTATUS SICCSA');
	            		context.siccsa_estado_cod=null;
	            		context.siccsa_estado_des=null;
	            		context.siccsa_estado_obs=null;
	            		context.siccsa_fecha_compromiso=null;
	            		context.siccsa_quien_paga=null;
	            		context.siccsa_tiene_volante=null;
	            		context.siccsa_tiene_declaracion_aduanera=null;
	            		context.siccsa_tiene_numeracion=null;
	            		context.siccsa_es_sli=null;
	            		context.siccsa_age_adu_scharff=null;
	            		context.siccsa_se_entrego_docs=null;
	            		context.siccsa_derechos_monto=null;
	            		context.siccsa_derechos_moneda=null;
	            		context.siccsa_gastos_monto=null;
	            		context.siccsa_gastos_moneda=null;
	            		context.siccsa_cambio_regimen=null;;
	            	}
	            }
	            callback(null, context);
		  	} catch(e) {
		  		console.log("Error en estadoSiccsa WS 002",e);
		  		callback(null, context);
		  	}
		  }
		});
    },
    estadoSintad: function(context, callback) {
    	var guia = context.guia;
    	var numero = context.guia_nro;
    	var anio = context.guia_anio;

    	var options = {
		    uri: 'https://scharff-int-core.mybluemix.net/api/scharff/estadosintad?anio=' + anio +'&numero='+numero+'&guia='+guia,
		    method: 'GET',
		    timeout: 80000
		}
    	request(options, function (error, response, body) {
		  if (error || response.statusCode != 200) {
		  	console.log("Error en estadoSintad WS 001");
		  	callback(null, context);
		  } else {
		  	try {
		  		var bodyJ = JSON.parse(body);
		  		console.log('body',bodyJ);
				if (bodyJ.error) {
					console.log('ERROR EN WEBSERVICE MANUAL');
					context.sintad_estado_cod=null;
            		context.sintad_estado_des=null;
            		context.sintad_estado_obs=null;
            		context.sintad_derechos_monto=null;
            		context.sintad_derechos_moneda=null;
            		context.sintad_gastos_monto=null;
            		context.sintad_gastos_moneda=null;
	            } else {
	            	if(bodyJ[0].ESTADO_COD && bodyJ[0].ESTADO_COD != ''){
	            		console.log('TIENE ESTATUS SINTAD');
	            		context.sintad_estado_cod=bodyJ[0].ESTADO_COD;
	            		context.sintad_estado_des=bodyJ[0].ESTADO_DES;
	            		context.sintad_estado_obs=bodyJ[0].ESTADO_OBS;
	            		context.sintad_derechos_monto=bodyJ[0].DERECHOS_MONTO;
	            		context.sintad_derechos_moneda=bodyJ[0].DERECHOS_MONEDA;
	            		context.sintad_gastos_monto=bodyJ[0].GASTOS_MONTO;
	            		context.sintad_gastos_moneda=bodyJ[0].GASTOS_MONEDA;
	            	}else{
	            		console.log('NO TIENE ESTATUS SINTAD');
	            		context.sintad_estado_cod=null;
	            		context.sintad_estado_des=null;
	            		context.sintad_estado_obs=null;
	            		context.sintad_derechos_monto=null;
	            		context.sintad_derechos_moneda=null;
	            		context.sintad_gastos_monto=null;
	            		context.sintad_gastos_moneda=null;
	            	}
	            }
	            callback(null, context);
		  	} catch(e) {
		  		console.log("Error en estadoFedex WS 002",e);
		  		callback(null, context);
		  	}
		  }
		});
    },
    afterConversation: function(message, conversationResponse, callback) {
    	var log = new Log(db);
        try {
          if (conversationResponse.context){
              console.log("Log: Saving Log", JSON.stringify(conversationResponse));

              log.codigo_fb = conversationResponse.context.codigo_fb;
              log.nombre_fb = conversationResponse.context.nombre_fb;
              log.apellido_fb = conversationResponse.context.apellido_fb;
              log.foto_fb = conversationResponse.context.foto_fb;
              //log.codigo_fb = "None";
              log.conversation_id = conversationResponse.context.conversation_id;
              log.categoria = conversationResponse.context.main;
              log.subcategoria = conversationResponse.context.submain;
              log.detalle = conversationResponse.context.detail;
              log.canal = 'facebook';

              if (conversationResponse.input.text) {
                  log.user_text = conversationResponse.input.text;
                } else {
                  log.user_text = "None";
                }
                if (conversationResponse.output.text) {
                  if (Array.isArray(conversationResponse.output.text)) {
                      log.watson_response = conversationResponse.output.text.join(' ');
                  } else {
                      log.watson_response = conversationResponse.output.text;
                  }
                } else {
                    log.watson_response = "None";
                }
                if (conversationResponse.output.webservice){
                	log.webservice = conversationResponse.output.webservice;
                }else{
                	log.webservice = "None";
                }
                if (conversationResponse.intents[0]) {
                    log.intent = conversationResponse.intents[0].intent;
                    log.confidence = conversationResponse.intents[0].confidence;
                } else {
                    log.intent = "None";
                    log.confidence = '0.00';
                }
          log.save();
        }
        callback(null, conversationResponse);
        } catch(e) {
                console.log('Log: Error Saving Log');
                console.log(e);
        	callback(null, conversationResponse);
        }
    },
    beforeConversation: function(message, conversationPayload, callback){
    	//console.log("before Payload: ",conversationPayload);
    	//console.log("before: ",message);

    	console.log('cliente: ', message.user);

    	if(typeof conversationPayload.context == 'undefined'){
    		conversationPayload.context = {};
    	}

    	if(typeof conversationPayload.context !== 'undefined'){
    		if((typeof conversationPayload.context.nombre_fb == 'undefined') || !conversationPayload.context.nombre_fb){
    			var id_facebook = message.user;
				var options = {
					headers: {
				      'Content-Type': 'application/json',
				      'Accept':'application/json',
				      //'Authorization': 'Bearer DQVJ0LXNVMEhrUzBzZAThsNmtsTVkxaUFQOHh6b2QyU2Vad0hJSWRQZADBoVG1POVBKLTBpRHlhdEx0bFB0S0pSd1dtZAXpuYUduQVhndW8wZAnlaWDR1aEFBcnpWcVRiYTBicEJFWGdsckxRS21aTEV2TTJFck5fWWNuVnM1a0g2ZAmZARcWhjUUY5b2xuLVZAfRUpMRHhNYkZAOVVd2MjQ0eDdyT1lZAV3pTX2RiRTFyeGMyNjdqQmRWVVFIbFJYUTNoQ2MyUVJtaDBn'
				    },
				    uri: 'https://graph.facebook.com/'+id_facebook+'?name&access_token='+process.env.FB_ACCESS_TOKEN,
				    method: 'GET',
				    timeout: 80000
				};

				request(options, function (error, response, body) {
					  if (error || response.statusCode != 200) {
						  	console.log("Error en Servicio Perfil Facebook WS 001");
						  	conversationPayload.context.error = 'info';
						  	callback(null, conversationPayload);
					  } else {
						  	try {
						  		var bodyCliente = JSON.parse(body);
						  		console.log("Respuesta Servicio Perfil Facebook: ", bodyCliente);
						  		if (typeof bodyCliente != undefined && bodyCliente != null){

						  			if(bodyCliente.first_name != null && (typeof bodyCliente.first_name != 'undefined')){
						  				conversationPayload.context.nombre_fb= bodyCliente.first_name;
						  			}else{
						  				conversationPayload.context.nombre_fb= '';
						  			}

						  			if(bodyCliente.last_name != null && (typeof bodyCliente.last_name != 'undefined')){
						  				conversationPayload.context.apellido_fb= bodyCliente.last_name;
						  			}else{
						  				conversationPayload.context.apellido_fb= '';
						  			}

						  			if(bodyCliente.id != null && (typeof bodyCliente.id != 'undefined')){
						  				conversationPayload.context.codigo_fb= bodyCliente.id;
						  			}else{
						  				conversationPayload.context.codigo_fb= message.user;
						  			}

						  			if(bodyCliente.profile_pic != null && (typeof bodyCliente.profile_pic != 'undefined')){
						  				conversationPayload.context.foto_fb= bodyCliente.profile_pic;
						  			}else{
						  				conversationPayload.context.foto_fb= '';
						  			}

						  		}else{
						  			console.log('Error Respuesta Servicio Perfil Facebook Indefinida o Vacia');
						  			conversationPayload.context.error = 'info';
						  			conversationPayload.context.nombre_fb= '';
						  			conversationPayload.context.apellido_fb='';
						  			conversationPayload.context.codigo_fb=message.user;
						  			conversationPayload.context.foto_fb='';
						  		}

					            callback(null, conversationPayload);
						  	} catch(e) {
						  		console.log("Error in Servicio Perfil Facebook WS 002");
						  		console.log(e);
						  		conversationPayload.context.error = 'info';
					  			conversationPayload.context.nombre_fb= '';
					  			conversationPayload.context.apellido_fb='';
					  			conversationPayload.context.codigo_fb=message.user;
					  			conversationPayload.context.foto_fb='';
						  		callback(null, conversationPayload);
						  	}
					  }
					});

    		}else{
    			console.log('Nombres de cliente facebook seteado en contexto');
	    		callback(null, conversationPayload);
    		}
    	}else{
    		console.log('Sin contexto');
    		callback(null, conversationPayload);
    	}

    	//conversationPayload.context.codigo_fb = message.user;
    	//conversationPayload.context.name_workplace='Yersy'
    	//conversationPayload.context.modalidad_alumno='PREGRADO';
    	//callback(null, conversationPayload);
    	
    }
};
