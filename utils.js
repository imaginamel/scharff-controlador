var sendMail = function(context) {
	const sgMail = require('@sendgrid/mail');
	sgMail.setApiKey(process.env.SENDGRID_API_KEY);
  	if (context.user_aprobado === 'true') {
		const msg = {
		  to: context.user_correo,
		  from: 'efectiva@brunotafur.com',
		  fromname: 'Equipo Efectivo',
		  subject: 'Préstamo Pre Aprobado Financiera Efectiva',
		  text: 'Hola' + context.user_nombre + ', estamos muy contentos de que te encuentres en la lista de pre aprobados. Nuestra meta es ser la solución financiera de las familias peruanas. Los datos del préstamo que tienes aprobados son: Monto máximo: S/ ' + context.user_monto_aprobado + 'Plazo máximo: ' + context.user_plazo_maximo + ' meses. Hay dos formas de obtenerlo. (i) Espera la llamada de nuestro Equipo Efectivo o (ii) Acércate hoy mismo con tu DNI y este correo electrónico a una de nuestras oficinas ubicadas en Tiendas Efe y La Curacao.  Para ver donde están ubicadas haz clic aquí. Pregunta por nuestro Funcionario de Negocios quien te atenderá gustosamente y te ayudará a obtener tu dinero rápidamente. Sólo debes imprimir o mostrarle desde tu celular este correo electrónico. Esta pre aprobación sólo tiene una vigencia por 03 días y está sujeta a que los clientes mantengan la situación financiera al momento de la evaluación en las bases de datos de Financiera Efectiva. Te esperamos en nuestras oficinas! Nuestro horario de atención es de Lunes a Domingo de 9:00 a.m. a 9:00 p.m. Gracias por contactarte con Financiera Efectiva. Cordialmente, El Equipo Efectivo',
		  html: '<p>Hola ' + context.user_nombre + ', estamos muy contentos de que te encuentres en la lista de pre aprobados. Nuestra meta es ser la solución financiera de las familias peruanas. </p><p>Los datos del préstamo que tienes aprobados son: </p> <ul><li>Monto máximo: S/ ' + context.user_monto_aprobado + ' </li><li>Plazo máximo: ' + context.user_plazo_maximo + ' meses</li></ul> <p>Hay dos formas de obtenerlo. (i) Espera la llamada de nuestro Equipo Efectivo o (ii) Acércate hoy mismo con tu DNI y este correo electrónico a una de nuestras oficinas ubicadas en Tiendas Efe y La Curacao.  Para ver donde están ubicadas haz clic aquí. </p><p>Pregunta por nuestro Funcionario de Negocios quien te atenderá gustosamente y te ayudará a obtener tu dinero rápidamente. Sólo debes imprimir o mostrarle desde tu celular este correo electrónico.</p> <p>Esta pre aprobación sólo tiene una vigencia por 03 días y está sujeta a que los clientes mantengan la situación financiera al momento de la evaluación en las bases de datos de Financiera Efectiva.</p><p>Te esperamos en nuestras oficinas! Nuestro horario de atención es de Lunes a Domingo de 9:00 a.m. a 9:00 p.m.</p><p>Gracias por contactarte con Financiera Efectiva.</p><p>Cordialmente, </p><p>El Equipo Efectivo</p>'
		};
		sgMail.send(msg);
  	} else if (context.user_aprobado === 'false') {
		const msg = {
		  to: context.user_correo,
		  from: 'efectiva@brunotafur.com',
		  fromname: 'Equipo Efectivo',
		  subject: 'Solicitud de préstamo de Financiera Efectiva',
		  text: 'Hola ' + context.user_nombre + ', agradecemos tu interés en tomar un préstamo con nosotros.  Nuestra meta es ser la solución financiera de las familias peruanas.  Tenemos préstamos hasta S/6,000 y puedes pagarlo hasta en 24 meses. Somos parte del Grupo Efe, contamos con productos para el hogar en Tiendas Efe y La Curacao. También tenemos tiendas especializadas en motos a través de nuestra cadena Motocorp.  Allí podrás encontrar lo último para que lo disfrutes en familia. Visítanos en efe.com.pe / lacuracao.com.pe /motocorp.pe. En el transcurso de 24 horas un asesor de servicio se contactará contigo para solicitarte los datos para tu evaluación crediticia. Si deseas ser evaluado hoy, acércate con tu DNI y un recibo de servicios, a una de nuestras oficinas ubicadas en Tiendas Efe y La Curacao. Para ver donde están ubicadas haz clic aquí. Pregunta por nuestro Funcionario de Negocios quien te atenderá gustosamente y ayudará a obtener tu dinero rápidamente. Te esperamos en nuestras oficinas! Nuestro horario de atención es de Lunes a Domingo de 9:00 a.m. a 9:00 p.m. Gracias por contactarte con Financiera Efectiva. Nuevamente te felicitamos y te esperamos en nuestras oficinas. Cordialmente, El Equipo Efectivo',
		  html: '<p>Hola ' + context.user_nombre + ', agradecemos tu interés en tomar un préstamo con nosotros.  Nuestra meta es ser la solución financiera de las familias peruanas.  Tenemos préstamos hasta S/6,000 y puedes pagarlo hasta en 24 meses.</p><p>Somos parte del Grupo Efe, contamos con productos para el hogar en Tiendas Efe y La Curacao. También tenemos tiendas especializadas en motos a través de nuestra cadena Motocorp.  Allí podrás encontrar lo último para que lo disfrutes en familia.<br> Visítanos en <a href="http://efe.com.pe" target="_blank">efe.com.pe</a> / <a href="http://lacuracao.com.pe" target="_blank">lacuracao.com.pe</a> /<a href="http://motocorp.pe" target="_blank">motocorp.pe</a></p><p>En el transcurso de 24 horas un asesor de servicio se contactará contigo para solicitarte los datos para tu evaluación crediticia. Si deseas ser evaluado hoy, acércate con tu DNI y un recibo de servicios, a una de nuestras oficinas ubicadas en Tiendas Efe y La Curacao. Para ver donde están ubicadas haz clic aquí.</p><p>Pregunta por nuestro Funcionario de Negocios quien te atenderá gustosamente y ayudará a obtener tu dinero rápidamente.</p><p>Te esperamos en nuestras oficinas! Nuestro horario de atención es de Lunes a Domingo de 9:00 a.m. a 9:00 p.m.<br>Gracias por contactarte con Financiera Efectiva.</p><p>Nuevamente te felicitamos y te esperamos en nuestras oficinas</p><p>Cordialmente, </p><p>El Equipo Efectivo</p>'
		};
		sgMail.send(msg);
  	}
}
var verifyCredentialsWeb = function(req, res, workspace) {
	if (!req.headers.apikey || !req.headers.appsecret) {
    return res.json({
      'output': {
        'text': 'Credenciales incorrectas.'
      }
    });
  }
  if (req.headers.apikey !== process.env.API_KEY || req.headers.appsecret !== process.env.APP_SECRET) {
    return res.json({
      'output': {
        'text': 'Credenciales incorrectas.'
      }
    });
  } 
  if (!workspace || workspace === '<workspace-id>') {
    return res.json({
      'output': {
        'text': 'No se ha especificado un WORKSPACE_ID en las variables de entorno.'
      }
    });
  }
}

var parseButtonsToHTML = function(jsonData, jsonElements) {
	var html = '<p>' + jsonData.join('\n') + '</p>';
	html += '<ul class="watson-chatbot-slider">';
	for(var i in jsonElements) {
		var element = jsonElements[i];
		html += '<li class="watson-chatbot-element">'; 
		if (element.default_action) {
			if (element.default_action.type === "web_url") {
				html += '<a target="_blank" href="' + element.default_action.url + '">'; 
			} 
		}
		html += '<div class="watson-chatbot-img"><img src="' + element.image_url + '"/></div>';
		html += '<div class="watson-chatbot-desc"><h4>' + element.title + '</h4>';
		html += '<h5>' + element.subtitle + '</h5></div>';
		if (element.buttons) {
			html += '<div class="watson-chatbot-buttons">'; 
			for (var b in element.buttons) {
			    var button = element.buttons[b];
			    if (button.type === "postback") {
			    	html += '<div class="watson-postback active watson-button">'; 
			    } else {
			        html += '<div>'; 
			    }
			    html += button.title; 
			    html += '</div>';
			}
		    html += '</div>'; 
		}
		if (element.default_action) {
			if (element.default_action.type == "web_url") {
			    html += '</a>'; 
			}
		} 
		html += '</li>';
	}
	html += '</ul>';
	return html;
}
var clone = function(a) { return JSON.parse(JSON.stringify(a)); }

module.exports.sendMail = sendMail;
module.exports.parseButtonsToHTML = parseButtonsToHTML;
module.exports.verifyCredentialsWeb = verifyCredentialsWeb;
module.exports.clone = clone;