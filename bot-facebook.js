module.exports = function (watsonMiddleware) {
    var request = require('request');
    var Botkit = require('botkit');
    var services = require('./services');

    var controller = Botkit.facebookbot({
      access_token: process.env.FB_ACCESS_TOKEN,
      verify_token: process.env.FB_VERIFY_TOKEN,
      receive_via_postback: true
      //json_file_store: __dirname + '/data/'
    });


    var bot = controller.spawn();

    var Promise = require('bluebird');
    Promise.promisifyAll(watsonMiddleware);
    Promise.promisifyAll(services);
    Promise.promisifyAll(bot);
    var utils = require('./utils');
    var chitchat = require('./data/chitchat.json');

    var processWatsonResponse = function (bot, message) {

      //console.log('Mensaj de Facebook: ', message);
      //console.log('Bot',JSON.stringify(bot));

        if (message.watsonError) {
            console.log("Error en el procesamiento: ",message);
            return bot.reply(message, "Disculpa pero por motivos técnicos no he podido responder tu consulta. Por favor intenta nuevamente :) ");
            /*return bot.replyAsync(message, "Disculpa pero por motivos técnicos no he podido responder tu consulta. Por favor intenta nuevamente.").then(function () {
                return bot.reply(message, "prueba");
              });*/
        }

        if (!message.text) {
          console.log('Primer if');
          console.log('Mensaje: ', JSON.stringify(message));
            if (message.sticker_id) {
                controller.trigger('sticker_received', [bot, message]);
                return false;
            } else if (message.attachments && message.attachments[0]) {
                controller.trigger(message.attachments[0].type + '_received', [bot, message]);
                return false;
            }
        }


        if (typeof message.watsonData.output !== 'undefined') {

            /* Frontend Parse and Send Message*/
            console.log("Mensaje de Watson: ", JSON.stringify(message));

            if (message.watsonData.output.nodes_visited && !message.watsonData.output.action) {
              var visited = message.watsonData.output.nodes_visited;
              var nodeName = visited[visited.length-1];
              if (nodeName) {
                  if (chitchat[nodeName]) {
                      message.watsonData.output.text = chitchat[nodeName][Math.floor(Math.random() * chitchat[nodeName].length)];
                      bot.reply(message, message.watsonData.output.text);
                  }else{
                      bot.reply(message, message.watsonData.output.text.join('\n'));
                  }
              }
            }

            /*Elementos de Facebook (Buttons, Sliders, etc)*/
            var response = {};
            if (message.watsonData.output.action === 'ACTION_show_buttons') {
                response = {
                    "attachment": {
                        'type':'template',
                        'payload':{
                            'template_type':'button',
                            'text':message.watsonData.output.text.join('\n'),
                            'buttons': message.watsonData.output.buttons
                        }
                    }
                };
                bot.reply(message, response );
            }else if (message.watsonData.output.action === 'ACTION_show_carousel'){
                bot.reply(message,message.watsonData.output.text.join('\n'));
                response = {
                    "attachment": {
                        'type':'template',
                        'payload':{
                            'template_type':'generic',
                            'elements': message.watsonData.output.elements
                        }
                    }
                };
                bot.reply(message, response );
            }else if (message.watsonData.output.action === 'ACTION_show_list'){
                bot.reply(message,message.watsonData.output.text.join('\n'));
                response = {
                    "attachment": {
                        'type':'template',
                        'payload':{
                            'template_type':'list',
                            'top_element_style': 'compact',
                            'elements': message.watsonData.output.elements
                        }
                    }
                };
                bot.reply(message, response );
            } else if (message.watsonData.output.action === 'ACTION_show_quick_replies'){
                response = {
                  "text": message.watsonData.output.text.join('\n'),
                  "quick_replies":[
                    {
                      "content_type":"text",
                      "title":"Search",
                      "payload":"Search paylaod",
                      "image_url":"http://epe.upc.edu.pe/sites/default/files/upc/epe/sub_secciones/es/acerca_de_epe/edificios.jpg"
                    },
                    {
                      "content_type":"location"
                    },
                    {
                      "content_type":"text",
                      "title":"Something Else",
                      "payload":"something payload"
                    }
                  ]
                };
                bot.reply(message, response );
            } else if (message.watsonData.output.action === 'ACTION_show_image'){
                bot.reply(message,message.watsonData.output.text.join('\n'));
                response = {
                    "attachment":{
                      "type":"image",
                      "payload":{
                        "url":message.watsonData.output.url
                      }
                    }
                };
                bot.reply(message, response );
            }
            
            
            /* Backend Services */
            if (message.watsonData.output.action_backend === "ACTION_validar_por_guia") {
              var newMessage = utils.clone(message);
              //newMessage.text = 'obteniendo_creditos_estado';
              services.validarPorGuiaAsync(message.watsonData.context).then(function (contextDelta) {
                return watsonMiddleware.sendToWatsonAsync(bot, newMessage, contextDelta);
              }).catch(function (error) {
                newMessage.watsonError = error;
                return processWatsonResponse(bot, newMessage);
              }).then(function () {
                return processWatsonResponse(bot, newMessage);
              });
            }else if (message.watsonData.output.action_backend === "ACTION_validar_por_persona") {
              var newMessage = utils.clone(message);
              //newMessage.text = 'obteniendo_creditos_estado';
              services.validarPorPersonaAsync(message.watsonData.context).then(function (contextDelta) {
                return watsonMiddleware.sendToWatsonAsync(bot, newMessage, contextDelta);
              }).catch(function (error) {
                newMessage.watsonError = error;
                return processWatsonResponse(bot, newMessage);
              }).then(function () {
                return processWatsonResponse(bot, newMessage);
              });
            }else if (message.watsonData.output.action_backend === "ACTION_validar_por_empresa") {
              var newMessage = utils.clone(message);
              //newMessage.text = 'obteniendo_creditos_estado';
              services.validarPorEmpresaAsync(message.watsonData.context).then(function (contextDelta) {
                return watsonMiddleware.sendToWatsonAsync(bot, newMessage, contextDelta);
              }).catch(function (error) {
                newMessage.watsonError = error;
                return processWatsonResponse(bot, newMessage);
              }).then(function () {
                return processWatsonResponse(bot, newMessage);
              });
            }else if (message.watsonData.output.action_backend === "ACTION_estado_fedex") {
              var newMessage = utils.clone(message);
              //newMessage.text = 'obteniendo_riesgo_academico';
              services.estadoFedexAsync(message.watsonData.context).then(function (contextDelta) {
                return watsonMiddleware.sendToWatsonAsync(bot, newMessage, contextDelta);
              }).catch(function (error) {
                newMessage.watsonError = error;
                return processWatsonResponse(bot, newMessage);
              }).then(function () {
                return processWatsonResponse(bot, newMessage);
              });
            }else if (message.watsonData.output.action_backend === "ACTION_estado_siccsa") {
              var newMessage = utils.clone(message);
              //newMessage.text = 'obteniendo_riesgo_academico';
              services.estadoSiccsaAsync(message.watsonData.context).then(function (contextDelta) {
                return watsonMiddleware.sendToWatsonAsync(bot, newMessage, contextDelta);
              }).catch(function (error) {
                newMessage.watsonError = error;
                return processWatsonResponse(bot, newMessage);
              }).then(function () {
                return processWatsonResponse(bot, newMessage);
              });
            }else if (message.watsonData.output.action_backend === "ACTION_estado_sintad") {
              var newMessage = utils.clone(message);
              //newMessage.text = 'obteniendo_riesgo_academico';
              services.estadoSintadAsync(message.watsonData.context).then(function (contextDelta) {
                return watsonMiddleware.sendToWatsonAsync(bot, newMessage, contextDelta);
              }).catch(function (error) {
                newMessage.watsonError = error;
                return processWatsonResponse(bot, newMessage);
              }).then(function () {
                return processWatsonResponse(bot, newMessage);
              });
            }else if (message.watsonData.output.action_backend === "ACTION_enviar_alerta") {
              var newMessage = utils.clone(message);
              //newMessage.text = 'obteniendo_riesgo_academico';
              services.enviarAlertaAsync(message.watsonData.context).then(function (contextDelta) {
                return watsonMiddleware.sendToWatsonAsync(bot, newMessage, contextDelta);
              }).catch(function (error) {
                newMessage.watsonError = error;
                return processWatsonResponse(bot, newMessage);
              }).then(function () {
                return processWatsonResponse(bot, newMessage);
              });
            }else if (message.watsonData.output.action_backend === "ACTION_riesgo_academico") {
              var newMessage = utils.clone(message);
              //newMessage.text = 'obteniendo_riesgo_academico';
              services.checkRiesgoAcademicoAsync(message.watsonData.context).then(function (contextDelta) {
                return watsonMiddleware.sendToWatsonAsync(bot, newMessage, contextDelta);
              }).catch(function (error) {
                newMessage.watsonError = error;
                return processWatsonResponse(bot, newMessage);
              }).then(function () {
                return processWatsonResponse(bot, newMessage);
              });
            }
        }
    };

    controller.on('sticker_received', function(bot, message) {
      bot.reply(message, 'Sticker recibido 🤖');
    });

    controller.on('image_received', function(bot, message) {
        bot.reply(message, 'Imagen recibida 🤖');
    });

    controller.on('audio_received', function(bot, message) {
        bot.reply(message, 'Es genial escucharte, pero por ahora sólo puedo leerte :(. Pronto podré entender lo que me dices por voz ;) ');
    });

    controller.on('file_received', function(bot, message) {
        bot.reply(message, 'Los archivos son muy interesantes, pronto podré saber de qué tratan :)');
    });

    //controller.hears('(.*)', 'message_received', processWatsonResponse);

    controller.on('message_received', processWatsonResponse);

    controller.api.messenger_profile.get_started('Get started');
    /*controller.api.thread_settings.menu([{
        "locale":"default",
        "composer_input_disabled":false,
        "call_to_actions":[
            {
                "title":"Matricula",
                "type":"nested",
                "call_to_actions":[
                    {
                        "title":"¿Como me matriculo?",
                        "type":"postback",
                        "payload":"Proceso matricula"
                    },
                    {
                        "title":"Mi turno de matrícula",
                        "type":"postback",
                        "payload":"turno de matricula"
                    },
                    {
                        "title":"Problemas de Matrícula",
                        "type":"postback",
                        "payload":"problemas de matricula"
                    }
                ]
            },
            {
                "title":"Pago",
                "type":"nested",
                "call_to_actions":[
                    {
                        "title":"Facilidades de Pago",
                        "type":"postback",
                        "payload":"Facilidades de pago"
                    },
                    {
                        "title":"Mi categoría de Pago",
                        "type":"postback",
                        "payload":"Mi categoría de Pago"
                    },
                    {
                        "title":"Fechas de Pago",
                        "type":"postback",
                        "payload":"Fechas de Pago"
                    }
                ]
            },
            {
                
                "type":"postback",
                "title":"Calendario",
                "payload":"calendario"
            }
        ]
    }
]);
*/
   
    var module = {
        bot: bot,
        controller: controller
    };

    return module;
};