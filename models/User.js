const Promise = require('promise');
const moment = require('moment');

module.exports = class User {

	constructor(db) {
		this.db = db;
		this.cod_alumno = "";
		this.cod_usuario = "";
		this.nombres = "";
		this.apellido_pat = "";
		this.apellido_mat = "";
		this.created_at = moment().format('YYYY-MM-DD HH:mm:ss');
	}
	static find (db, codigo) {
		return new Promise(function(resolve, reject) {
			if (codigo) {
				db.get('upc_bot.user',['id','cod_alumno','cod_usuario'],['cod_alumno','=',codigo]).then(function(user){
					resolve(user[0]);
				}).catch(function(err){
					resolve(null);
				})
			} else {
				reject(null);
			}
		});
	}
	save() {
		var slf = this;
		return new Promise(function(resolve, reject) {
			if (slf.cod_alumno) {
				slf.db.insert('upc_bot.user',{
					'cod_alumno': slf.cod_alumno,
					'cod_usuario': slf.cod_usuario,
					'nombres': slf.nombres,
					'apellido_pat': slf.apellido_pat,
					'apellido_mat': slf.apellido_mat,
					'created_at': slf.created_at
				}).then(function(val){
					resolve(val);
				}).catch(function(err){
					reject(null);
				});;
			} else {
				reject(null);
			}
		});
	}

}