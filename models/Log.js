const Promise = require('promise');
const moment = require('moment');

require('moment-timezone');
moment.tz.setDefault("America/Lima");

module.exports = class Log {

	constructor(db){
		this.db = db;
		this.codigo_fb = "";
		this.nombre_fb= "";
		this.apellido_fb = "";
		this.foto_fb = "";
		this.conversation_id="";
		this.user_text = "";
		this.watson_response = "";
		this.intent = "";
		this.confidence = "";
		this.categoria = "";
		this.subcategoria = "";
		this.detalle = "";
		this.sede = "";
		this.webservice = "";
		this.canal="",
		this.created_at = moment().format('YYYY-MM-DD HH:mm:ss');
	}

	save(){
		var slf = this;
		if (slf.conversation_id && slf.watson_response && slf.intent && slf.confidence && slf.created_at) {
			slf.db.insert('scharff_bot.logs',{
				'codigo_fb': slf.codigo_fb,
				'nombre_fb': slf.nombre_fb,
				'apellido_fb': slf.apellido_fb,
				'foto_fb': slf.foto_fb,
				'conversation_id': slf.conversation_id,
				'user_text': slf.user_text,
				'watson_response': slf.watson_response,
				'intent': slf.intent,
				'confidence': slf.confidence,
				'categoria': slf.categoria,
				'subcategoria': slf.subcategoria,
				'detalle': slf.detalle,
				'webservice': slf.webservice,
				'canal':slf.canal,
				'created_at': slf.created_at
			});
		} 
	}
}