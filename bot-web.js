var express = require('express');
var bodyParser = require('body-parser');
var verify = require('./security');
var app = express();
app.use( express.static( './public' ) ); 

var Conversation = require('watson-developer-cloud/conversation/v1');
var conversation = new Conversation({
  url: 'https://gateway.watsonplatform.net/conversation/api',
  username: process.env.CONVERSATION_USERNAME,
  password: process.env.CONVERSATION_PASSWORD,
  version_date: '2016-10-21',
  version: 'v1'
});

var jsonParser = bodyParser.json();
var workspace = process.env.WORKSPACE_ID || '<workspace-id>';
var Promise = require('bluebird');
var services = require('./services');
var utils = require('./utils');
Promise.promisifyAll(services);

var processWatsonResponse = function (conversation, payload, res, req) {
    console.log("PETITION");
    console.log(payload);
    conversation.message(payload, function(err, data) {
        
        console.log("RESPONSE1");
        if (err) {
          console.log("Error en Watson");
          var payload = {
            workspace_id: workspace,
            output: { text: 'Disculpa pero por motivos técnicos no he podido responder tu consulta. Por favor intenta nuevamente.' }
          };
          return res.json(payload);
        }
        if (data.output.action === 'ACTION_show_buttons') {
            data.output.text = utils.parseButtonsToHTML(data.output.text, data.output.elements);
        } else if (data.output.action === 'ACTION_show_carousel') {
            data.output.text = utils.parseButtonsToHTML(data.output.text, data.output.elements);
        }

        if (data.output.action_backend === "ACTION_validar_por_guia") {
            
            services.validarPorGuiaAsync(data.context).then(function (contextDelta) {
                var payload = {
                    workspace_id: workspace,
                    context: data.context || {},
                    input: { text: 'validando_guia' }
                };
                return processWatsonResponse(conversation, payload, res, req);
            }).catch(function (error) {
                var payload = {
                    workspace_id: workspace,
                    context: data.context || {},
                    input: { text: 'validando_guia' }
                };
                return processWatsonResponse(conversation, payload, res, req);
            });
        } else if (data.output.action_backend === "ACTION_validar_por_persona") {
            
            services.validarPorPersonaAsync(data.context).then(function (contextDelta) {
                var payload = {
                    workspace_id: workspace,
                    context: data.context || {},
                    input: { text: 'validando_aprobacion' }
                };
                return processWatsonResponse(conversation, payload, res, req);
            }).catch(function (error) {
                var payload = {
                    workspace_id: workspace,
                    context: data.context || {},
                    input: { text: 'validando_aprobacion' }
                };
                return processWatsonResponse(conversation, payload, res, req);
            });
        } else if (data.output.action_backend === "ACTION_validar_por_empresa") {
            
            services.validarPorEmpresaAsync(data.context).then(function (contextDelta) {
                var payload = {
                    workspace_id: workspace,
                    context: data.context || {},
                    input: { text: 'validando_aprobacion' }
                };
                return processWatsonResponse(conversation, payload, res, req);
            }).catch(function (error) {
                var payload = {
                    workspace_id: workspace,
                    context: data.context || {},
                    input: { text: 'validando_aprobacion' }
                };
                return processWatsonResponse(conversation, payload, res, req);
            });
        } else if (data.output.action_backend === "ACTION_estado_fedex") {
            
            services.estadoFedexAsync(data.context).then(function (contextDelta) {
                var payload = {
                    workspace_id: workspace,
                    context: data.context || {},
                    input: { text: 'validando_aprobacion' }
                };
                return processWatsonResponse(conversation, payload, res, req);
            }).catch(function (error) {
                var payload = {
                    workspace_id: workspace,
                    context: data.context || {},
                    input: { text: 'validando_aprobacion' }
                };
                return processWatsonResponse(conversation, payload, res, req);
            });
        } else if (data.output.action_backend === "ACTION_estado_siccsa") {
            
            services.estadoSiccsaAsync(data.context).then(function (contextDelta) {
                var payload = {
                    workspace_id: workspace,
                    context: data.context || {},
                    input: { text: 'validando_aprobacion' }
                };
                return processWatsonResponse(conversation, payload, res, req);
            }).catch(function (error) {
                var payload = {
                    workspace_id: workspace,
                    context: data.context || {},
                    input: { text: 'validando_aprobacion' }
                };
                return processWatsonResponse(conversation, payload, res, req);
            });
        } else if (data.output.action_backend === "ACTION_estado_sintad") {
            
            services.estadoSintadAsync(data.context).then(function (contextDelta) {
                var payload = {
                    workspace_id: workspace,
                    context: data.context || {},
                    input: { text: 'validando_aprobacion' }
                };
                return processWatsonResponse(conversation, payload, res, req);
            }).catch(function (error) {
                var payload = {
                    workspace_id: workspace,
                    context: data.context || {},
                    input: { text: 'validando_aprobacion' }
                };
                return processWatsonResponse(conversation, payload, res, req);
            });
        } else if (data.output.action_backend === "ACTION_enviar_alerta") {
            
            services.enviarAlertaAsync(data.context).then(function (contextDelta) {
                var payload = {
                    workspace_id: workspace,
                    context: data.context || {},
                    input: { text: 'validando_aprobacion' }
                };
                return processWatsonResponse(conversation, payload, res, req);
            }).catch(function (error) {
                var payload = {
                    workspace_id: workspace,
                    context: data.context || {},
                    input: { text: 'validando_aprobacion' }
                };
                return processWatsonResponse(conversation, payload, res, req);
            });
        } else {
          services.afterConversation(req.body.input, data, function(status, payload) {
            console.log("AFTER RESPONSE");
            return res.json(payload);
          });
        }
    });
}

app.post('/api/message', jsonParser, function (req, res) {
    //utils.verifyCredentialsWeb(req, res, workspace);
    var payload = {
        workspace_id: workspace,
        context: req.body.context || {},
        input: req.body.input || {}
    };
    return processWatsonResponse(conversation, payload, res, req);
});

app.use(bodyParser.json({
    verify: verify
}));

module.exports = app;
