require('dotenv').load();

var port = process.env.PORT || 5500;
var app  = require('./bot-web');

app.set('port', port);
require('./app')(app);
var Store = require("jfs");
var db = new Store("data");

app.listen(port, function() {
  console.log('Client server listening on port ' + port);
});
