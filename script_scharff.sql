create table scharff_bot.categoria (
	id serial primary key,
	nombre varchar(40),
	descripcion varchar(60)

);

create table scharff_bot.subcategoria (
	id serial primary key,
	id_categoria int,
	nombre varchar(40),
	descripcion varchar(60),
	foreign key (id_categoria) references scharff_bot.categoria(id)
);

create table scharff_bot.admin (
      id serial primary key,
      usuario varchar(30) unique,
      password varchar(150)
);


create table scharff_bot.logs (
    id serial primary key,
    conversation_id VARCHAR(60),
    codigo_fb VARCHAR(100),
    user_text text,
    watson_response text,
	intent varchar(50),
    canal varchar(20),
	confidence decimal(5, 2),
	categoria varchar(60),
	subcategoria varchar(60),
	detalle varchar(60),
	webservice varchar(60),
	created_at timestamp
);

ALTER TABLE scharff_bot.logs ADD COLUMN nombre_fb VARCHAR (200);
ALTER TABLE scharff_bot.logs ADD COLUMN apellido_fb VARCHAR (200);
ALTER TABLE scharff_bot.logs ADD COLUMN foto_fb TEXT;

insert into scharff_bot.categoria(id,nombre,descripcion) values 
	(1,'estatus','Estatus'),
	(2,'recojo','Recojo'),
	(3,'tarifas','Tarifas'),
	(4,'contacto','Contacto'),
	(5,'reclamos','Reclamos'),
	--(6,'mensajes_recojo','Mensajes de recojo'),
	--(7,'tnt','TNT'),
    (8,'no_entiendo','No Entiendo');

insert into scharff_bot.subcategoria(id_categoria,nombre,descripcion) values 
	(1,'paquete','Estatus Paquete'),
	(1,'recojo','Estatus recojo'),
	(1,'consulta_monto_recojo','Monto recojo'),
	(1,'volante','Estatus Volante'),
	(1,'reclamo','Reclamo Estatus'),
	(1,'fecha_entrega','Fecha Entrega'),
	(2,'programacion','Programación Recojo'),
	(2,'tiempo','Tiempo Recojo'),
	(2,'dudas_envio','Dudas Envío'),
	(3,'consulta_costo','Cotizacion Envío'),
	(3,'consulta_costo_almacenaje','Costo Almacenaje'),
	(3,'dudas_costos','Dudas de Costos'),
	(4,'general','Contacto General'),
	(4,'rrhh','RR.HH.'),
	(4,'persona','Contacto Personas');

    
-- Vistas

------------------------------------------------------------------------------------------------------------------------
----Contadores Donut (DIA gmailTUAL)
/*
create view scharff_bot.v_contadoresDonutGeneral_Dia as
(select 'canal' as tipo, D.codigo, D.nombre, D.cantidad, D.total,
CASE
    WHEN D.total = 0 THEN D.total
    ELSE coalesce(round(cast((cast(D.cantidad as float)/cast(D.total as float)) as numeric),2),0)*100
END as porcentaje
from
(select codigo, nombre, coalesce(count,0) as cantidad,
(select count(*) from scharff_bot.logs where (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date = (created_at)::date ) as total 
from 
(select * from scharff_bot.canal
LEFT JOIN
(select canal, count(*)
from scharff_bot.logs 
where (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date = (created_at)::date 
group by canal) B on scharff_bot.canal.codigo = B.canal) C) D)
UNION
(select 'sede' as tipo, D.codigo, D.nombre, D.cantidad, D.total,
CASE
    WHEN D.total = 0 THEN D.total
    ELSE coalesce(round(cast((cast(D.cantidad as float)/cast(D.total as float)) as numeric),2),0)*100
END as porcentaje
from
(select codigo, nombre, coalesce(count,0) as cantidad,
(select count(*) from scharff_bot.logs where (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date = (created_at)::date ) as total 
from 
(select * from scharff_bot.sede
LEFT JOIN
(select sede, count(*)
from scharff_bot.logs 
where (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date = (created_at)::date 
group by sede) B on scharff_bot.sede.codigo = B.sede) C) D)
UNION
(select 'sexo' as tipo, D.codigo, D.nombre, D.cantidad, D.total,
CASE
    WHEN D.total = 0 THEN D.total
    ELSE coalesce(round(cast((cast(D.cantidad as float)/cast(D.total as float)) as numeric),2),0)*100
END as porcentaje
from
(select codigo, nombre, coalesce(count,0) as cantidad,
(select count(*) from scharff_bot.logs where (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date = (created_at)::date ) as total 
from 
(select * from scharff_bot.sexo
LEFT JOIN
(select sexo, count(*)
from scharff_bot.logs 
where (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date = (created_at)::date 
group by sexo) B on scharff_bot.sexo.codigo = B.sexo) C) D);

----Contadores Donut (SEMANA gmailTUAL)
create view scharff_bot.v_contadoresDonutGeneral_Semana as
(select 'canal' as tipo, D.codigo, D.nombre, D.cantidad, D.total,
CASE
    WHEN D.total = 0 THEN D.total
    ELSE coalesce(round(cast((cast(D.cantidad as float)/cast(D.total as float)) as numeric),2),0)*100
END as porcentaje
from
(select codigo, nombre, coalesce(count,0) as cantidad,
(select count(*) from scharff_bot.logs 
 where date_trunc('week',created_at)=date_trunc('week', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date) ) as total 
from 
(select * from scharff_bot.canal
LEFT JOIN
(select canal, count(*)
from scharff_bot.logs 
where date_trunc('week',created_at)=date_trunc('week', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)
group by canal) B on scharff_bot.canal.codigo = B.canal) C) D)
UNION
(select 'sede' as tipo, D.codigo, D.nombre, D.cantidad, D.total,
CASE
    WHEN D.total = 0 THEN D.total
    ELSE coalesce(round(cast((cast(D.cantidad as float)/cast(D.total as float)) as numeric),2),0)*100
END as porcentaje
from
(select codigo, nombre, coalesce(count,0) as cantidad,
(select count(*) from scharff_bot.logs 
where date_trunc('week',created_at)=date_trunc('week', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date) ) as total 
from 
(select * from scharff_bot.sede
LEFT JOIN
(select sede, count(*)
from scharff_bot.logs 
where date_trunc('week',created_at)=date_trunc('week', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)
group by sede) B on scharff_bot.sede.codigo = B.sede) C) D)
UNION
(select 'sexo' as tipo, D.codigo, D.nombre, D.cantidad, D.total,
CASE
    WHEN D.total = 0 THEN D.total
    ELSE coalesce(round(cast((cast(D.cantidad as float)/cast(D.total as float)) as numeric),2),0)*100
END as porcentaje
from
(select codigo, nombre, coalesce(count,0) as cantidad,
(select count(*) from scharff_bot.logs 
 where date_trunc('week',created_at)=date_trunc('week', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date) ) as total 
from 
(select * from scharff_bot.sexo
LEFT JOIN
(select sexo, count(*)
from scharff_bot.logs 
where date_trunc('week',created_at)=date_trunc('week', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)
group by sexo) B on scharff_bot.sexo.codigo = B.sexo) C) D);

----Contadores Donut (MES gmailTUAL)
create view scharff_bot.v_contadoresDonutGeneral_Mes as
(select 'canal' as tipo, D.codigo, D.nombre, D.cantidad, D.total,
CASE
    WHEN D.total = 0 THEN D.total
    ELSE coalesce(round(cast((cast(D.cantidad as float)/cast(D.total as float)) as numeric),2),0)*100
END as porcentaje
from
(select codigo, nombre, coalesce(count,0) as cantidad,
(select count(*) from scharff_bot.logs 
 where date_trunc('month',created_at)=date_trunc('month', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date) ) as total 
from 
(select * from scharff_bot.canal
LEFT JOIN
(select canal, count(*)
from scharff_bot.logs 
where date_trunc('month',created_at)=date_trunc('month', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)
group by canal) B on scharff_bot.canal.codigo = B.canal) C) D)
UNION
(select 'sede' as tipo, D.codigo, D.nombre, D.cantidad, D.total,
CASE
    WHEN D.total = 0 THEN D.total
    ELSE coalesce(round(cast((cast(D.cantidad as float)/cast(D.total as float)) as numeric),2),0)*100
END as porcentaje
from
(select codigo, nombre, coalesce(count,0) as cantidad,
(select count(*) from scharff_bot.logs 
where date_trunc('month',created_at)=date_trunc('month', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date) ) as total 
from 
(select * from scharff_bot.sede
LEFT JOIN
(select sede, count(*)
from scharff_bot.logs 
where date_trunc('month',created_at)=date_trunc('month', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)
group by sede) B on scharff_bot.sede.codigo = B.sede) C) D)
UNION
(select 'sexo' as tipo, D.codigo, D.nombre, D.cantidad, D.total,
CASE
    WHEN D.total = 0 THEN D.total
    ELSE coalesce(round(cast((cast(D.cantidad as float)/cast(D.total as float)) as numeric),2),0)*100
END as porcentaje
from
(select codigo, nombre, coalesce(count,0) as cantidad,
(select count(*) from scharff_bot.logs 
 where date_trunc('month',created_at)=date_trunc('month', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date) ) as total 
from 
(select * from scharff_bot.sexo
LEFT JOIN
(select sexo, count(*)
from scharff_bot.logs 
where date_trunc('month',created_at)=date_trunc('month', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)
group by sexo) B on scharff_bot.sexo.codigo = B.sexo) C) D);

-----FUNCTION CONTADORES DONUT RANGO FECHAS
--select * from scharff_bot.contadoresDonutGeneral_Rango('2018-01-31','2018-02-15');

CREATE OR REPLACE FUNCTION scharff_bot.contadoresDonutGeneral_Rango(start_date date, end_date date)
  RETURNS TABLE (
  tipo text,
  codigo varchar,
  nombre varchar,
  cantidad bigint,
  total bigint,
  porcentaje numeric) as
$BODY$
BEGIN 
   RETURN QUERY
   
select H.tipo, H.codigo, H.nombre, H.cantidad, H.total, H.porcentaje from (   
(select 'canal' as tipo, D.codigo, D.nombre, D.cantidad, D.total,
CASE
    WHEN D.total = 0 THEN D.total
    ELSE coalesce(round(cast((cast(D.cantidad as float)/cast(D.total as float)) as numeric),2),0)*100
END as porcentaje
from
(select C.codigo, C.nombre, coalesce(C.count,0) as cantidad,
(select count(*) from scharff_bot.logs 
 where created_at >= start_date and created_at <= end_date ) as total 
from 
(select * from scharff_bot.canal
LEFT JOIN
(select canal, count(*)
from scharff_bot.logs 
where created_at >= start_date and created_at <= end_date
group by canal) B on scharff_bot.canal.codigo = B.canal) C) D)
UNION
(select 'sede' as tipo, D.codigo, D.nombre, D.cantidad, D.total,
CASE
    WHEN D.total = 0 THEN D.total
    ELSE coalesce(round(cast((cast(D.cantidad as float)/cast(D.total as float)) as numeric),2),0)*100
END as porcentaje
from
(select C.codigo, C.nombre, coalesce(C.count,0) as cantidad,
(select count(*) from scharff_bot.logs 
 where created_at >= start_date and created_at <= end_date ) as total 
from 
(select * from scharff_bot.sede
LEFT JOIN
(select sede, count(*)
from scharff_bot.logs 
where created_at >= start_date and created_at <= end_date
group by sede) B on scharff_bot.sede.codigo = B.sede) C) D)
UNION
(select 'sexo' as tipo, D.codigo, D.nombre, D.cantidad, D.total,
CASE
    WHEN D.total = 0 THEN D.total
    ELSE coalesce(round(cast((cast(D.cantidad as float)/cast(D.total as float)) as numeric),2),0)*100
END as porcentaje
from
(select C.codigo, C.nombre, coalesce(C.count,0) as cantidad,
(select count(*) from scharff_bot.logs 
 where created_at >= start_date and created_at <= end_date ) as total 
from 
(select * from scharff_bot.sexo
LEFT JOIN
(select sexo, count(*)
from scharff_bot.logs 
where created_at >= start_date and created_at <= end_date
group by sexo) B on scharff_bot.sexo.codigo = B.sexo) C) D)) H;
    
END;
$BODY$ 
LANGUAGE plpgsql;

-------------------------------------------------

---Top Donuts General (DIA gmailTUAL)
create view scharff_bot.v_contadoresDonutTop_Dia as
(select 'canal' as tipo, D.codigo, D.nombre, D.cantidad, D.total,
CASE
    WHEN D.total = 0 THEN D.total
    ELSE coalesce(round(cast((cast(D.cantidad as float)/cast(D.total as float)) as numeric),2),0)*100
END as porcentaje
from
(select codigo, nombre, coalesce(count,0) as cantidad,
(select count(*) from scharff_bot.logs where (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date = (created_at)::date ) as total 
from 
(select * from scharff_bot.canal
LEFT JOIN
(select canal, count(*)
from scharff_bot.logs 
where (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date = (created_at)::date 
group by canal) B on scharff_bot.canal.codigo = B.canal) C) D order by cantidad desc limit 1)
UNION
(select 'sede' as tipo, D.codigo, D.nombre, D.cantidad, D.total,
CASE
    WHEN D.total = 0 THEN D.total
    ELSE coalesce(round(cast((cast(D.cantidad as float)/cast(D.total as float)) as numeric),2),0)*100
END as porcentaje
from
(select codigo, nombre, coalesce(count,0) as cantidad,
(select count(*) from scharff_bot.logs where (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date = (created_at)::date ) as total 
from 
(select * from scharff_bot.sede
LEFT JOIN
(select sede, count(*)
from scharff_bot.logs 
where (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date = (created_at)::date 
group by sede) B on scharff_bot.sede.codigo = B.sede) C) D order by cantidad desc limit 1)
UNION
(select 'sexo' as tipo, D.codigo, D.nombre, D.cantidad, D.total,
CASE
    WHEN D.total = 0 THEN D.total
    ELSE coalesce(round(cast((cast(D.cantidad as float)/cast(D.total as float)) as numeric),2),0)*100
END as porcentaje
from
(select codigo, nombre, coalesce(count,0) as cantidad,
(select count(*) from scharff_bot.logs where (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date = (created_at)::date ) as total 
from 
(select * from scharff_bot.sexo
LEFT JOIN
(select sexo, count(*)
from scharff_bot.logs 
where (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date = (created_at)::date 
group by sexo) B on scharff_bot.sexo.codigo = B.sexo) C) D order by cantidad desc limit 1);

---Top Donuts General (SEMANA gmailTUAL)
create view scharff_bot.v_contadoresDonutTop_Semana as
(select 'canal' as tipo, D.codigo, D.nombre, D.cantidad, D.total,
CASE
    WHEN D.total = 0 THEN D.total
    ELSE coalesce(round(cast((cast(D.cantidad as float)/cast(D.total as float)) as numeric),2),0)*100
END as porcentaje
from
(select codigo, nombre, coalesce(count,0) as cantidad,
(select count(*) from scharff_bot.logs 
 where date_trunc('week',created_at)=date_trunc('week', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date) ) as total 
from 
(select * from scharff_bot.canal
LEFT JOIN
(select canal, count(*)
from scharff_bot.logs 
where date_trunc('week',created_at)=date_trunc('week', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)
group by canal) B on scharff_bot.canal.codigo = B.canal) C) D order by cantidad desc limit 1)
UNION
(select 'sede' as tipo, D.codigo, D.nombre, D.cantidad, D.total,
CASE
    WHEN D.total = 0 THEN D.total
    ELSE coalesce(round(cast((cast(D.cantidad as float)/cast(D.total as float)) as numeric),2),0)*100
END as porcentaje
from
(select codigo, nombre, coalesce(count,0) as cantidad,
(select count(*) from scharff_bot.logs 
where date_trunc('week',created_at)=date_trunc('week', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date) ) as total 
from 
(select * from scharff_bot.sede
LEFT JOIN
(select sede, count(*)
from scharff_bot.logs 
where date_trunc('week',created_at)=date_trunc('week', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)
group by sede) B on scharff_bot.sede.codigo = B.sede) C) D order by cantidad desc limit 1)
UNION
(select 'sexo' as tipo, D.codigo, D.nombre, D.cantidad, D.total,
CASE
    WHEN D.total = 0 THEN D.total
    ELSE coalesce(round(cast((cast(D.cantidad as float)/cast(D.total as float)) as numeric),2),0)*100
END as porcentaje
from
(select codigo, nombre, coalesce(count,0) as cantidad,
(select count(*) from scharff_bot.logs 
 where date_trunc('week',created_at)=date_trunc('week', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date) ) as total 
from 
(select * from scharff_bot.sexo
LEFT JOIN
(select sexo, count(*)
from scharff_bot.logs 
where date_trunc('week',created_at)=date_trunc('week', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)
group by sexo) B on scharff_bot.sexo.codigo = B.sexo) C) D order by cantidad desc limit 1);

---Top Donuts General (MES gmailTUAL)
create view scharff_bot.v_contadoresDonutTop_Mes as
(select 'canal' as tipo, D.codigo, D.nombre, D.cantidad, D.total,
CASE
    WHEN D.total = 0 THEN D.total
    ELSE coalesce(round(cast((cast(D.cantidad as float)/cast(D.total as float)) as numeric),2),0)*100
END as porcentaje
from
(select codigo, nombre, coalesce(count,0) as cantidad,
(select count(*) from scharff_bot.logs 
 where date_trunc('month',created_at)=date_trunc('month', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date) ) as total 
from 
(select * from scharff_bot.canal
LEFT JOIN
(select canal, count(*)
from scharff_bot.logs 
where date_trunc('month',created_at)=date_trunc('month', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)
group by canal) B on scharff_bot.canal.codigo = B.canal) C) D order by cantidad desc limit 1)
UNION
(select 'sede' as tipo, D.codigo, D.nombre, D.cantidad, D.total,
CASE
    WHEN D.total = 0 THEN D.total
    ELSE coalesce(round(cast((cast(D.cantidad as float)/cast(D.total as float)) as numeric),2),0)*100
END as porcentaje
from
(select codigo, nombre, coalesce(count,0) as cantidad,
(select count(*) from scharff_bot.logs 
where date_trunc('month',created_at)=date_trunc('month', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date) ) as total 
from 
(select * from scharff_bot.sede
LEFT JOIN
(select sede, count(*)
from scharff_bot.logs 
where date_trunc('month',created_at)=date_trunc('month', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)
group by sede) B on scharff_bot.sede.codigo = B.sede) C) D order by cantidad desc limit 1)
UNION
(select 'sexo' as tipo, D.codigo, D.nombre, D.cantidad, D.total,
CASE
    WHEN D.total = 0 THEN D.total
    ELSE coalesce(round(cast((cast(D.cantidad as float)/cast(D.total as float)) as numeric),2),0)*100
END as porcentaje
from
(select codigo, nombre, coalesce(count,0) as cantidad,
(select count(*) from scharff_bot.logs 
 where date_trunc('month',created_at)=date_trunc('month', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date) ) as total 
from 
(select * from scharff_bot.sexo
LEFT JOIN
(select sexo, count(*)
from scharff_bot.logs 
where date_trunc('month',created_at)=date_trunc('month', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)
group by sexo) B on scharff_bot.sexo.codigo = B.sexo) C) D order by cantidad desc limit 1);

----TOP DONUT GENERAL RANGO FECHAS

--select * from scharff_bot.contadoresDonutTop_Rango('2018-02-03','2018-02-05')

CREATE OR REPLACE FUNCTION scharff_bot.contadoresDonutTop_Rango(start_date date, end_date date)
  RETURNS TABLE (
  tipo text,
  codigo varchar,
  nombre varchar,
  cantidad bigint,
  total bigint,
  porcentaje numeric) as
$BODY$
BEGIN 
   RETURN QUERY
   
select H.tipo, H.codigo, H.nombre, H.cantidad, H.total, H.porcentaje from (   
(select 'canal' as tipo, D.codigo, D.nombre, D.cantidad, D.total,
CASE
    WHEN D.total = 0 THEN D.total
    ELSE coalesce(round(cast((cast(D.cantidad as float)/cast(D.total as float)) as numeric),2),0)*100
END as porcentaje
from
(select C.codigo, C.nombre, coalesce(C.count,0) as cantidad,
(select count(*) from scharff_bot.logs 
 where created_at >= start_date and created_at <= end_date ) as total 
from 
(select * from scharff_bot.canal
LEFT JOIN
(select canal, count(*)
from scharff_bot.logs 
where created_at >= start_date and created_at <= end_date
group by canal) B on scharff_bot.canal.codigo = B.canal) C) D order by cantidad desc limit 1)
UNION
(select 'sede' as tipo, D.codigo, D.nombre, D.cantidad, D.total,
CASE
    WHEN D.total = 0 THEN D.total
    ELSE coalesce(round(cast((cast(D.cantidad as float)/cast(D.total as float)) as numeric),2),0)*100
END as porcentaje
from
(select C.codigo, C.nombre, coalesce(C.count,0) as cantidad,
(select count(*) from scharff_bot.logs 
 where created_at >= start_date and created_at <= end_date ) as total 
from 
(select * from scharff_bot.sede
LEFT JOIN
(select sede, count(*)
from scharff_bot.logs 
where created_at >= start_date and created_at <= end_date
group by sede) B on scharff_bot.sede.codigo = B.sede) C) D order by cantidad desc limit 1)
UNION
(select 'sexo' as tipo, D.codigo, D.nombre, D.cantidad, D.total,
CASE
    WHEN D.total = 0 THEN D.total
    ELSE coalesce(round(cast((cast(D.cantidad as float)/cast(D.total as float)) as numeric),2),0)*100
END as porcentaje
from
(select C.codigo, C.nombre, coalesce(C.count,0) as cantidad,
(select count(*) from scharff_bot.logs 
 where created_at >= start_date and created_at <= end_date ) as total 
from 
(select * from scharff_bot.sexo
LEFT JOIN
(select sexo, count(*)
from scharff_bot.logs 
where created_at >= start_date and created_at <= end_date
group by sexo) B on scharff_bot.sexo.codigo = B.sexo) C) D order by cantidad desc limit 1)) H;
    
END;
$BODY$ 
LANGUAGE plpgsql;

*/

------------------------------------------------------------------------------------------------------------------------

----Categorias por canal (DIA gmailTUAL)
create view scharff_bot.v_categoriascanal_Dia as 
SELECT A.nombre as categoria, A.descripcion as nombre, coalesce(B.facebook,0) as facebook, coalesce(C.gmail,0) as gmail from
(select nombre, descripcion from scharff_bot.categoria) A
LEFT JOIN 
(select categoria, count(*) as facebook from scharff_bot.logs where canal ='facebook' and categoria!= '' and categoria is not null
and (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date = (created_at)::date 
group by categoria) B ON A.nombre = B.categoria
LEFT JOIN
(select categoria, count(*)as gmail from scharff_bot.logs where canal ='gmail' and categoria!= '' and categoria is not null
and (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date = (created_at)::date 
group by categoria) C ON A.nombre = C.categoria;

---Categorias por canal (SEMANA gmailTUAL)
create view scharff_bot.v_categoriascanal_Semana as 
SELECT A.nombre as categoria,A.descripcion as nombre, coalesce(B.facebook,0) as facebook, coalesce(C.gmail,0) as gmail from
(select nombre,descripcion from scharff_bot.categoria) A
LEFT JOIN 
(select categoria, count(*) as facebook from scharff_bot.logs where canal ='facebook' and categoria!= '' and categoria is not null
and date_trunc('week',created_at)=date_trunc('week', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)
group by categoria) B ON A.nombre = B.categoria
LEFT JOIN
(select categoria, count(*)as gmail from scharff_bot.logs where canal ='gmail' and categoria!= '' and categoria is not null
and date_trunc('week',created_at)=date_trunc('week', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)
group by categoria) C ON A.nombre = C.categoria;

---Categorias por canal (MES gmailTUAL)

create view scharff_bot.v_categoriascanal_Mes as 
SELECT A.nombre as categoria, A.descripcion as nombre, coalesce(B.facebook,0) as facebook, coalesce(C.gmail,0) as gmail from
(select nombre,descripcion from scharff_bot.categoria) A
LEFT JOIN 
(select categoria, count(*) as facebook from scharff_bot.logs where canal ='facebook' and categoria!= '' and categoria is not null
and date_trunc('month',created_at)=date_trunc('month', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)
group by categoria) B ON A.nombre = B.categoria
LEFT JOIN
(select categoria, count(*)as gmail from scharff_bot.logs where canal ='gmail' and categoria!= '' and categoria is not null
and date_trunc('month',created_at)=date_trunc('month', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)
group by categoria) C ON A.nombre = C.categoria;

---Categorias por canal (RANGO)
--select * from scharff_bot.categoriascanal_Rango('2018-02-11','2018-02-22')

CREATE OR REPLACE FUNCTION scharff_bot.categoriascanal_Rango(start_date date, end_date date)
  RETURNS TABLE (
  categoria varchar,
  nombre varchar,
  facebook bigint,
  gmail bigint) as
$BODY$
BEGIN 
RETURN QUERY

SELECT A.nombre as categoria, A.descripcion as nombre, coalesce(B.facebook,0) as facebook, coalesce(C.gmail,0) as gmail from
(select E.nombre,E.descripcion from scharff_bot.categoria E) A
LEFT JOIN 
(select L.categoria, count(L.*) as facebook from scharff_bot.logs L where L.canal ='facebook' and L.categoria!= '' and L.categoria is not null
and L.created_at >= start_date and L.created_at <= end_date
group by L.categoria) B ON A.nombre = B.categoria
LEFT JOIN
(select L.categoria, count(L.*)as gmail from scharff_bot.logs L where L.canal ='gmail' and L.categoria!= '' and L.categoria is not null
and L.created_at >= start_date and L.created_at <= end_date
group by L.categoria) C ON A.nombre = C.categoria;
    
END;
$BODY$ 
LANGUAGE plpgsql;


------------------------------------------------------------
-----Mensajes por Categoria (DIA ACTUAL)
create view scharff_bot.v_mensajesCategoria_Dia as
SELECT Y.descripcion as categoria,coalesce(X.cantidad,0) as cantidad, coalesce(Y.sum,0)as total,
CASE
    WHEN Y.sum = 0 THEN Y.SUM
    ELSE coalesce(round(cast((cast(X.cantidad as float)/cast(Y.sum as float)) as numeric),2),0)*100
END as porcentaje
FROM
(select * from
(select nombre, descripcion from scharff_bot.categoria) C
CROSS JOIN
(select sum(A.cantidad) from
(select categoria, count(*) as cantidad from scharff_bot.logs where categoria in (select nombre from scharff_bot.categoria)
and (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date = (created_at)::date
group by categoria) A) B) Y
LEFT JOIN
(select categoria, count(*) as cantidad from scharff_bot.logs where categoria!= '' and categoria is not null
and (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date = (created_at)::date
group by categoria) X on X.categoria = Y.nombre;

-----Mensajes por Categoria (SEMANA ACTUAL)

create view scharff_bot.v_mensajesCategoria_Semana as 
SELECT Y.descripcion as categoria,coalesce(X.cantidad,0) as cantidad, coalesce(Y.sum,0)as total,
CASE
    WHEN Y.sum = 0 THEN Y.SUM
    ELSE coalesce(round(cast((cast(X.cantidad as float)/cast(Y.sum as float)) as numeric),2),0)*100
END as porcentaje
FROM
(select * from
(select descripcion, nombre from scharff_bot.categoria) C
CROSS JOIN
(select sum(A.cantidad) from
(select categoria, count(*) as cantidad from scharff_bot.logs where categoria in (select nombre from scharff_bot.categoria)
and date_trunc('week',created_at)=date_trunc('week', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)
group by categoria) A) B) Y
LEFT JOIN
(select categoria, count(*) as cantidad from scharff_bot.logs where categoria!= '' and categoria is not null
and date_trunc('week',created_at)=date_trunc('week', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)
group by categoria) X on X.categoria = Y.nombre;

-----Mensajes por Categoria (MES ACTUAL)
create view scharff_bot.v_mensajesCategoria_Mes as 
SELECT Y.descripcion as categoria,coalesce(X.cantidad,0) as cantidad, coalesce(Y.sum,0)as total,
CASE
    WHEN Y.sum = 0 THEN Y.SUM
    ELSE coalesce(round(cast((cast(X.cantidad as float)/cast(Y.sum as float)) as numeric),2),0)*100
END as porcentaje
FROM
(select * from
(select descripcion, nombre from scharff_bot.categoria) C
CROSS JOIN
(select sum(A.cantidad) from
(select categoria, count(*) as cantidad from scharff_bot.logs where categoria in (select nombre from scharff_bot.categoria)
and date_trunc('month',created_at)=date_trunc('month', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)
group by categoria) A) B) Y
LEFT JOIN
(select categoria, count(*) as cantidad from scharff_bot.logs where categoria!= '' and categoria is not null
and date_trunc('month',created_at)=date_trunc('month', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)
group by categoria) X on X.categoria = Y.nombre;

-----mensajes por Categoria (RANGO)

--select * from scharff_bot.mensajesCategoria_Rango('2018-02-11','2018-02-22')

CREATE OR REPLACE FUNCTION scharff_bot.mensajesCategoria_Rango(start_date date, end_date date)
  RETURNS TABLE (
  categoria varchar,
  cantidad int,
  total int,
  porcentaje numeric) as
$BODY$
BEGIN 
RETURN QUERY

SELECT Y.descripcion as categoria,coalesce(X.cantidad,0)::int as cantidad, coalesce(Y.sum,0)::int as total,
CASE
    WHEN Y.sum = 0 THEN Y.SUM
    ELSE coalesce(round(cast((cast(X.cantidad as float)/cast(Y.sum as float)) as numeric),2),0)*100
END as porcentaje
FROM
(select * from
(select descripcion, nombre from scharff_bot.categoria) C
CROSS JOIN
(select sum(A.cantidad) from
(select L.categoria, count(L.*) as cantidad from scharff_bot.logs L where L.categoria in (select nombre from scharff_bot.categoria)
and L.created_at >= start_date and L.created_at <= end_date
group by L.categoria) A) B) Y
LEFT JOIN
(select L.categoria, count(L.*) as cantidad from scharff_bot.logs L where L.categoria!= '' and L.categoria is not null
and L.created_at >= start_date and L.created_at <= end_date
group by L.categoria) X on X.categoria = Y.nombre;
    
END;
$BODY$ 
LANGUAGE plpgsql;

---------------------------------------------------------
---Mensajes Subcategoría (DIA gmailTUAL)

    
create view scharff_bot.v_mensajesSubcategoria_Dia as 
SELECT X.* FROM (
(SELECT Y.id_categoria, Y.descripcion as subcategoria,coalesce(X.cantidad,0)::int as cantidad, coalesce(Y.sum,0)::int as total,
CASE
    WHEN Y.sum = 0 THEN Y.SUM
    ELSE coalesce(round(cast((cast(X.cantidad as float)/cast(Y.sum as float)) as numeric),2),0)*100
END as porcentaje
FROM
(select * from
(select S.id_categoria,S.descripcion, S.nombre from scharff_bot.subcategoria S where S.id_categoria = 1) C
CROSS JOIN
(select sum(A.cantidad) from
(select L.subcategoria, count(L.*) as cantidad from scharff_bot.logs L where L.categoria ='estatus' and L.subcategoria in (select nombre from scharff_bot.subcategoria)
and (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date = (L.created_at)::date
group by L.subcategoria) A) B) Y
LEFT JOIN
(select L.subcategoria, count(L.*) as cantidad from scharff_bot.logs L where L.categoria ='estatus' and L.subcategoria in (select nombre from scharff_bot.subcategoria)
and (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date = (L.created_at)::date
group by L.subcategoria) X on X.subcategoria = Y.nombre)
UNION
(SELECT Y.id_categoria, Y.descripcion as subcategoria,coalesce(X.cantidad,0)::int as cantidad, coalesce(Y.sum,0)::int as total,
CASE
    WHEN Y.sum = 0 THEN Y.SUM
    ELSE coalesce(round(cast((cast(X.cantidad as float)/cast(Y.sum as float)) as numeric),2),0)*100
END as porcentaje
FROM
(select * from
(select S.id_categoria,S.descripcion, S.nombre from scharff_bot.subcategoria S where S.id_categoria = 2) C
CROSS JOIN
(select sum(A.cantidad) from
(select L.subcategoria, count(L.*) as cantidad from scharff_bot.logs L where L.categoria ='recojo' and L.subcategoria in (select nombre from scharff_bot.subcategoria)
and (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date = (L.created_at)::date
group by L.subcategoria) A) B) Y
LEFT JOIN
(select L.subcategoria, count(L.*) as cantidad from scharff_bot.logs L where L.categoria ='recojo' and L.subcategoria in (select nombre from scharff_bot.subcategoria)
and (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date = (L.created_at)::date
group by L.subcategoria) X on X.subcategoria = Y.nombre)
UNION
(SELECT Y.id_categoria, Y.descripcion as subcategoria,coalesce(X.cantidad,0)::int as cantidad, coalesce(Y.sum,0)::int as total,
CASE
    WHEN Y.sum = 0 THEN Y.SUM
    ELSE coalesce(round(cast((cast(X.cantidad as float)/cast(Y.sum as float)) as numeric),2),0)*100
END as porcentaje
FROM
(select * from
(select S.id_categoria,S.descripcion, S.nombre from scharff_bot.subcategoria S where S.id_categoria = 3) C
CROSS JOIN
(select sum(A.cantidad) from
(select L.subcategoria, count(L.*) as cantidad from scharff_bot.logs L where L.categoria ='tarifas' and L.subcategoria in (select nombre from scharff_bot.subcategoria)
and (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date = (L.created_at)::date
group by L.subcategoria) A) B) Y
LEFT JOIN
(select L.subcategoria, count(L.*) as cantidad from scharff_bot.logs L where L.categoria ='tarifas' and L.subcategoria in (select nombre from scharff_bot.subcategoria)
and (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date = (L.created_at)::date
group by L.subcategoria) X on X.subcategoria = Y.nombre)
UNION
(SELECT Y.id_categoria, Y.descripcion as subcategoria,coalesce(X.cantidad,0)::int as cantidad, coalesce(Y.sum,0)::int as total,
CASE
    WHEN Y.sum = 0 THEN Y.SUM
    ELSE coalesce(round(cast((cast(X.cantidad as float)/cast(Y.sum as float)) as numeric),2),0)*100
END as porcentaje
FROM
(select * from
(select S.id_categoria,S.descripcion, S.nombre from scharff_bot.subcategoria S where S.id_categoria = 4) C
CROSS JOIN
(select sum(A.cantidad) from
(select L.subcategoria, count(L.*) as cantidad from scharff_bot.logs L where L.categoria ='contacto' and L.subcategoria in (select nombre from scharff_bot.subcategoria)
and (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date = (L.created_at)::date
group by L.subcategoria) A) B) Y
LEFT JOIN
(select L.subcategoria, count(L.*) as cantidad from scharff_bot.logs L where L.categoria ='contacto' and L.subcategoria in (select nombre from scharff_bot.subcategoria)
and (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date = (L.created_at)::date
group by L.subcategoria) X on X.subcategoria = Y.nombre)
UNION
(SELECT Y.id_categoria, Y.descripcion as subcategoria,coalesce(X.cantidad,0)::int as cantidad, coalesce(Y.sum,0)::int as total,
CASE
    WHEN Y.sum = 0 THEN Y.SUM
    ELSE coalesce(round(cast((cast(X.cantidad as float)/cast(Y.sum as float)) as numeric),2),0)*100
END as porcentaje
FROM
(select * from
(select S.id_categoria,S.descripcion, S.nombre from scharff_bot.subcategoria S where S.id_categoria = 5) C
CROSS JOIN
(select sum(A.cantidad) from
(select L.subcategoria, count(L.*) as cantidad from scharff_bot.logs L where L.categoria ='reclamos' and L.subcategoria in (select nombre from scharff_bot.subcategoria)
and (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date = (L.created_at)::date
group by L.subcategoria) A) B) Y
LEFT JOIN
(select L.subcategoria, count(L.*) as cantidad from scharff_bot.logs L where L.categoria ='reclamos' and L.subcategoria in (select nombre from scharff_bot.subcategoria)
and (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date = (L.created_at)::date
group by L.subcategoria) X on X.subcategoria = Y.nombre))X order by X.id_categoria, X.subcategoria;

---Mensajes Subcategoría (SEMANA gmailTUAL)

    
create view scharff_bot.v_mensajesSubcategoria_Semana as 
SELECT X.* FROM (
(SELECT Y.id_categoria, Y.descripcion as subcategoria,coalesce(X.cantidad,0)::int as cantidad, coalesce(Y.sum,0)::int as total,
CASE
    WHEN Y.sum = 0 THEN Y.SUM
    ELSE coalesce(round(cast((cast(X.cantidad as float)/cast(Y.sum as float)) as numeric),2),0)*100
END as porcentaje
FROM
(select * from
(select S.id_categoria,S.descripcion, S.nombre from scharff_bot.subcategoria S where S.id_categoria = 1) C
CROSS JOIN
(select sum(A.cantidad) from
(select L.subcategoria, count(L.*) as cantidad from scharff_bot.logs L where L.categoria ='estatus' and L.subcategoria in (select nombre from scharff_bot.subcategoria)
and date_trunc('week',L.created_at)=date_trunc('week', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)
group by L.subcategoria) A) B) Y
LEFT JOIN
(select L.subcategoria, count(L.*) as cantidad from scharff_bot.logs L where L.categoria ='estatus' and L.subcategoria in (select nombre from scharff_bot.subcategoria)
and date_trunc('week',L.created_at)=date_trunc('week', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)
group by L.subcategoria) X on X.subcategoria = Y.nombre)
UNION
(SELECT Y.id_categoria, Y.descripcion as subcategoria,coalesce(X.cantidad,0)::int as cantidad, coalesce(Y.sum,0)::int as total,
CASE
    WHEN Y.sum = 0 THEN Y.SUM
    ELSE coalesce(round(cast((cast(X.cantidad as float)/cast(Y.sum as float)) as numeric),2),0)*100
END as porcentaje
FROM
(select * from
(select S.id_categoria,S.descripcion, S.nombre from scharff_bot.subcategoria S where S.id_categoria = 2) C
CROSS JOIN
(select sum(A.cantidad) from
(select L.subcategoria, count(L.*) as cantidad from scharff_bot.logs L where L.categoria ='recojo' and L.subcategoria in (select nombre from scharff_bot.subcategoria)
and date_trunc('week',L.created_at)=date_trunc('week', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)
group by L.subcategoria) A) B) Y
LEFT JOIN
(select L.subcategoria, count(L.*) as cantidad from scharff_bot.logs L where L.categoria ='recojo' and L.subcategoria in (select nombre from scharff_bot.subcategoria)
and date_trunc('week',L.created_at)=date_trunc('week', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)
group by L.subcategoria) X on X.subcategoria = Y.nombre)
UNION
(SELECT Y.id_categoria, Y.descripcion as subcategoria,coalesce(X.cantidad,0)::int as cantidad, coalesce(Y.sum,0)::int as total,
CASE
    WHEN Y.sum = 0 THEN Y.SUM
    ELSE coalesce(round(cast((cast(X.cantidad as float)/cast(Y.sum as float)) as numeric),2),0)*100
END as porcentaje
FROM
(select * from
(select S.id_categoria,S.descripcion, S.nombre from scharff_bot.subcategoria S where S.id_categoria = 3) C
CROSS JOIN
(select sum(A.cantidad) from
(select L.subcategoria, count(L.*) as cantidad from scharff_bot.logs L where L.categoria ='tarifas' and L.subcategoria in (select nombre from scharff_bot.subcategoria)
and date_trunc('week',L.created_at)=date_trunc('week', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)
group by L.subcategoria) A) B) Y
LEFT JOIN
(select L.subcategoria, count(L.*) as cantidad from scharff_bot.logs L where L.categoria ='tarifas' and L.subcategoria in (select nombre from scharff_bot.subcategoria)
and date_trunc('week',L.created_at)=date_trunc('week', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)
group by L.subcategoria) X on X.subcategoria = Y.nombre)
UNION
(SELECT Y.id_categoria, Y.descripcion as subcategoria,coalesce(X.cantidad,0)::int as cantidad, coalesce(Y.sum,0)::int as total,
CASE
    WHEN Y.sum = 0 THEN Y.SUM
    ELSE coalesce(round(cast((cast(X.cantidad as float)/cast(Y.sum as float)) as numeric),2),0)*100
END as porcentaje
FROM
(select * from
(select S.id_categoria,S.descripcion, S.nombre from scharff_bot.subcategoria S where S.id_categoria = 4) C
CROSS JOIN
(select sum(A.cantidad) from
(select L.subcategoria, count(L.*) as cantidad from scharff_bot.logs L where L.categoria ='contacto' and L.subcategoria in (select nombre from scharff_bot.subcategoria)
and date_trunc('week',L.created_at)=date_trunc('week', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)
group by L.subcategoria) A) B) Y
LEFT JOIN
(select L.subcategoria, count(L.*) as cantidad from scharff_bot.logs L where L.categoria ='contacto' and L.subcategoria in (select nombre from scharff_bot.subcategoria)
and date_trunc('week',L.created_at)=date_trunc('week', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)
group by L.subcategoria) X on X.subcategoria = Y.nombre)
UNION
(SELECT Y.id_categoria, Y.descripcion as subcategoria,coalesce(X.cantidad,0)::int as cantidad, coalesce(Y.sum,0)::int as total,
CASE
    WHEN Y.sum = 0 THEN Y.SUM
    ELSE coalesce(round(cast((cast(X.cantidad as float)/cast(Y.sum as float)) as numeric),2),0)*100
END as porcentaje
FROM
(select * from
(select S.id_categoria,S.descripcion, S.nombre from scharff_bot.subcategoria S where S.id_categoria = 5) C
CROSS JOIN
(select sum(A.cantidad) from
(select L.subcategoria, count(L.*) as cantidad from scharff_bot.logs L where L.categoria ='reclamos' and L.subcategoria in (select nombre from scharff_bot.subcategoria)
and date_trunc('week',L.created_at)=date_trunc('week', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)
group by L.subcategoria) A) B) Y
LEFT JOIN
(select L.subcategoria, count(L.*) as cantidad from scharff_bot.logs L where L.categoria ='reclamos' and L.subcategoria in (select nombre from scharff_bot.subcategoria)
and date_trunc('week',L.created_at)=date_trunc('week', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)
group by L.subcategoria) X on X.subcategoria = Y.nombre))X order by X.id_categoria, X.subcategoria;
---Mensajes Subcategoría (MES gmailTUAL)
    
create view scharff_bot.v_mensajesSubcategoria_Mes as 
SELECT X.* FROM (
(SELECT Y.id_categoria, Y.descripcion as subcategoria,coalesce(X.cantidad,0)::int as cantidad, coalesce(Y.sum,0)::int as total,
CASE
    WHEN Y.sum = 0 THEN Y.SUM
    ELSE coalesce(round(cast((cast(X.cantidad as float)/cast(Y.sum as float)) as numeric),2),0)*100
END as porcentaje
FROM
(select * from
(select S.id_categoria,S.descripcion, S.nombre from scharff_bot.subcategoria S where S.id_categoria = 1) C
CROSS JOIN
(select sum(A.cantidad) from
(select L.subcategoria, count(L.*) as cantidad from scharff_bot.logs L where L.categoria ='estatus' and L.subcategoria in (select nombre from scharff_bot.subcategoria)
and date_trunc('month',L.created_at)=date_trunc('month', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)
group by L.subcategoria) A) B) Y
LEFT JOIN
(select L.subcategoria, count(L.*) as cantidad from scharff_bot.logs L where L.categoria ='estatus' and L.subcategoria in (select nombre from scharff_bot.subcategoria)
and date_trunc('month',L.created_at)=date_trunc('month', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)
group by L.subcategoria) X on X.subcategoria = Y.nombre)
UNION
(SELECT Y.id_categoria, Y.descripcion as subcategoria,coalesce(X.cantidad,0)::int as cantidad, coalesce(Y.sum,0)::int as total,
CASE
    WHEN Y.sum = 0 THEN Y.SUM
    ELSE coalesce(round(cast((cast(X.cantidad as float)/cast(Y.sum as float)) as numeric),2),0)*100
END as porcentaje
FROM
(select * from
(select S.id_categoria,S.descripcion, S.nombre from scharff_bot.subcategoria S where S.id_categoria = 2) C
CROSS JOIN
(select sum(A.cantidad) from
(select L.subcategoria, count(L.*) as cantidad from scharff_bot.logs L where L.categoria ='recojo' and L.subcategoria in (select nombre from scharff_bot.subcategoria)
and date_trunc('month',L.created_at)=date_trunc('month', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)
group by L.subcategoria) A) B) Y
LEFT JOIN
(select L.subcategoria, count(L.*) as cantidad from scharff_bot.logs L where L.categoria ='recojo' and L.subcategoria in (select nombre from scharff_bot.subcategoria)
and date_trunc('month',L.created_at)=date_trunc('month', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)
group by L.subcategoria) X on X.subcategoria = Y.nombre)
UNION
(SELECT Y.id_categoria, Y.descripcion as subcategoria,coalesce(X.cantidad,0)::int as cantidad, coalesce(Y.sum,0)::int as total,
CASE
    WHEN Y.sum = 0 THEN Y.SUM
    ELSE coalesce(round(cast((cast(X.cantidad as float)/cast(Y.sum as float)) as numeric),2),0)*100
END as porcentaje
FROM
(select * from
(select S.id_categoria,S.descripcion, S.nombre from scharff_bot.subcategoria S where S.id_categoria = 3) C
CROSS JOIN
(select sum(A.cantidad) from
(select L.subcategoria, count(L.*) as cantidad from scharff_bot.logs L where L.categoria ='tarifas' and L.subcategoria in (select nombre from scharff_bot.subcategoria)
and date_trunc('month',L.created_at)=date_trunc('month', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)
group by L.subcategoria) A) B) Y
LEFT JOIN
(select L.subcategoria, count(L.*) as cantidad from scharff_bot.logs L where L.categoria ='tarifas' and L.subcategoria in (select nombre from scharff_bot.subcategoria)
and date_trunc('month',L.created_at)=date_trunc('month', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)
group by L.subcategoria) X on X.subcategoria = Y.nombre)
UNION
(SELECT Y.id_categoria, Y.descripcion as subcategoria,coalesce(X.cantidad,0)::int as cantidad, coalesce(Y.sum,0)::int as total,
CASE
    WHEN Y.sum = 0 THEN Y.SUM
    ELSE coalesce(round(cast((cast(X.cantidad as float)/cast(Y.sum as float)) as numeric),2),0)*100
END as porcentaje
FROM
(select * from
(select S.id_categoria,S.descripcion, S.nombre from scharff_bot.subcategoria S where S.id_categoria = 4) C
CROSS JOIN
(select sum(A.cantidad) from
(select L.subcategoria, count(L.*) as cantidad from scharff_bot.logs L where L.categoria ='contacto' and L.subcategoria in (select nombre from scharff_bot.subcategoria)
and date_trunc('month',L.created_at)=date_trunc('month', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)
group by L.subcategoria) A) B) Y
LEFT JOIN
(select L.subcategoria, count(L.*) as cantidad from scharff_bot.logs L where L.categoria ='contacto' and L.subcategoria in (select nombre from scharff_bot.subcategoria)
and date_trunc('month',L.created_at)=date_trunc('month', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)
group by L.subcategoria) X on X.subcategoria = Y.nombre)
UNION
(SELECT Y.id_categoria, Y.descripcion as subcategoria,coalesce(X.cantidad,0)::int as cantidad, coalesce(Y.sum,0)::int as total,
CASE
    WHEN Y.sum = 0 THEN Y.SUM
    ELSE coalesce(round(cast((cast(X.cantidad as float)/cast(Y.sum as float)) as numeric),2),0)*100
END as porcentaje
FROM
(select * from
(select S.id_categoria,S.descripcion, S.nombre from scharff_bot.subcategoria S where S.id_categoria = 5) C
CROSS JOIN
(select sum(A.cantidad) from
(select L.subcategoria, count(L.*) as cantidad from scharff_bot.logs L where L.categoria ='reclamos' and L.subcategoria in (select nombre from scharff_bot.subcategoria)
and date_trunc('month',L.created_at)=date_trunc('month', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)
group by L.subcategoria) A) B) Y
LEFT JOIN
(select L.subcategoria, count(L.*) as cantidad from scharff_bot.logs L where L.categoria ='reclamos' and L.subcategoria in (select nombre from scharff_bot.subcategoria)
and date_trunc('month',L.created_at)=date_trunc('month', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)
group by L.subcategoria) X on X.subcategoria = Y.nombre))X order by X.id_categoria, X.subcategoria;

---Mensajes subcategoria (RANGO)


--select * from scharff_bot.mensajesSubcategoria_Rango('2018-02-10','2018-02-15');

CREATE OR REPLACE FUNCTION scharff_bot.mensajesSubcategoria_Rango(start_date date, end_date date)
  RETURNS TABLE (
  id_categoria integer,
  subcategoria varchar,
  cantidad int,
  total int,
  porcentaje numeric) as
$BODY$
BEGIN 
RETURN QUERY

SELECT X.* FROM (
(SELECT Y.id_categoria, Y.descripcion as subcategoria,coalesce(X.cantidad,0)::int as cantidad, coalesce(Y.sum,0)::int as total,
CASE
    WHEN Y.sum = 0 THEN Y.SUM
    ELSE coalesce(round(cast((cast(X.cantidad as float)/cast(Y.sum as float)) as numeric),2),0)*100
END as porcentaje
FROM
(select * from
(select S.id_categoria,S.descripcion, S.nombre from scharff_bot.subcategoria S where S.id_categoria = 1) C
CROSS JOIN
(select sum(A.cantidad) from
(select L.subcategoria, count(L.*) as cantidad from scharff_bot.logs L where L.categoria ='estatus' and L.subcategoria in (select nombre from scharff_bot.subcategoria)
and L.created_at >= start_date and L.created_at <= end_date
group by L.subcategoria) A) B) Y
LEFT JOIN
(select L.subcategoria, count(L.*) as cantidad from scharff_bot.logs L where L.categoria ='estatus' and L.subcategoria in (select nombre from scharff_bot.subcategoria)
and L.created_at >= start_date and L.created_at <= end_date
group by L.subcategoria) X on X.subcategoria = Y.nombre)
UNION
(SELECT Y.id_categoria, Y.descripcion as subcategoria,coalesce(X.cantidad,0)::int as cantidad, coalesce(Y.sum,0)::int as total,
CASE
    WHEN Y.sum = 0 THEN Y.SUM
    ELSE coalesce(round(cast((cast(X.cantidad as float)/cast(Y.sum as float)) as numeric),2),0)*100
END as porcentaje
FROM
(select * from
(select S.id_categoria,S.descripcion, S.nombre from scharff_bot.subcategoria S where S.id_categoria = 2) C
CROSS JOIN
(select sum(A.cantidad) from
(select L.subcategoria, count(L.*) as cantidad from scharff_bot.logs L where L.categoria ='recojo' and L.subcategoria in (select nombre from scharff_bot.subcategoria)
and L.created_at >= start_date and L.created_at <= end_date
group by L.subcategoria) A) B) Y
LEFT JOIN
(select L.subcategoria, count(L.*) as cantidad from scharff_bot.logs L where L.categoria ='recojo' and L.subcategoria in (select nombre from scharff_bot.subcategoria)
and L.created_at >= start_date and L.created_at <= end_date
group by L.subcategoria) X on X.subcategoria = Y.nombre)
UNION
(SELECT Y.id_categoria, Y.descripcion as subcategoria,coalesce(X.cantidad,0)::int as cantidad, coalesce(Y.sum,0)::int as total,
CASE
    WHEN Y.sum = 0 THEN Y.SUM
    ELSE coalesce(round(cast((cast(X.cantidad as float)/cast(Y.sum as float)) as numeric),2),0)*100
END as porcentaje
FROM
(select * from
(select S.id_categoria,S.descripcion, S.nombre from scharff_bot.subcategoria S where S.id_categoria = 3) C
CROSS JOIN
(select sum(A.cantidad) from
(select L.subcategoria, count(L.*) as cantidad from scharff_bot.logs L where L.categoria ='tarifas' and L.subcategoria in (select nombre from scharff_bot.subcategoria)
and L.created_at >= start_date and L.created_at <= end_date
group by L.subcategoria) A) B) Y
LEFT JOIN
(select L.subcategoria, count(L.*) as cantidad from scharff_bot.logs L where L.categoria ='tarifas' and L.subcategoria in (select nombre from scharff_bot.subcategoria)
and L.created_at >= start_date and L.created_at <= end_date
group by L.subcategoria) X on X.subcategoria = Y.nombre)
UNION
(SELECT Y.id_categoria, Y.descripcion as subcategoria,coalesce(X.cantidad,0)::int as cantidad, coalesce(Y.sum,0)::int as total,
CASE
    WHEN Y.sum = 0 THEN Y.SUM
    ELSE coalesce(round(cast((cast(X.cantidad as float)/cast(Y.sum as float)) as numeric),2),0)*100
END as porcentaje
FROM
(select * from
(select S.id_categoria,S.descripcion, S.nombre from scharff_bot.subcategoria S where S.id_categoria = 4) C
CROSS JOIN
(select sum(A.cantidad) from
(select L.subcategoria, count(L.*) as cantidad from scharff_bot.logs L where L.categoria ='contacto' and L.subcategoria in (select nombre from scharff_bot.subcategoria)
and L.created_at >= start_date and L.created_at <= end_date
group by L.subcategoria) A) B) Y
LEFT JOIN
(select L.subcategoria, count(L.*) as cantidad from scharff_bot.logs L where L.categoria ='contacto' and L.subcategoria in (select nombre from scharff_bot.subcategoria)
and L.created_at >= start_date and L.created_at <= end_date
group by L.subcategoria) X on X.subcategoria = Y.nombre)
UNION
(SELECT Y.id_categoria, Y.descripcion as subcategoria,coalesce(X.cantidad,0)::int as cantidad, coalesce(Y.sum,0)::int as total,
CASE
    WHEN Y.sum = 0 THEN Y.SUM
    ELSE coalesce(round(cast((cast(X.cantidad as float)/cast(Y.sum as float)) as numeric),2),0)*100
END as porcentaje
FROM
(select * from
(select S.id_categoria,S.descripcion, S.nombre from scharff_bot.subcategoria S where S.id_categoria = 5) C
CROSS JOIN
(select sum(A.cantidad) from
(select L.subcategoria, count(L.*) as cantidad from scharff_bot.logs L where L.categoria ='reclamos' and L.subcategoria in (select nombre from scharff_bot.subcategoria)
and L.created_at >= start_date and L.created_at <= end_date
group by L.subcategoria) A) B) Y
LEFT JOIN
(select L.subcategoria, count(L.*) as cantidad from scharff_bot.logs L where L.categoria ='reclamos' and L.subcategoria in (select nombre from scharff_bot.subcategoria)
and L.created_at >= start_date and L.created_at <= end_date
group by L.subcategoria) X on X.subcategoria = Y.nombre))X order by X.id_categoria, X.subcategoria;
    
END;
$BODY$ 
LANGUAGE plpgsql;


------------------------------------------------------------
--aqui me quede
----Mensajes Categoria Line (DIA gmailTUAL)
create view scharff_bot.v_categoriaLine_Dia as 
SELECT estatus.hora_dia, estatus.estatus, recojo.recojo, tarifas.tarifas,contacto.contacto,
reclamos.reclamos, no_entiendo.no_entiendo FROM
(select to_char(to_timestamp((B.hora) * 60), 'MI:SS')as hora_dia, coalesce(A.count,0) as estatus from(
select date_part('hour',created_at) as hora, count(*) from scharff_bot.logs 
where ((now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date) = (created_at)::date
and categoria ='estatus'
group by date_part('hour',created_at)
) A right join(
select * from generate_series(0,23) as h(hora)
)B on A.hora = B.hora) estatus
LEFT JOIN
(select to_char(to_timestamp((B.hora) * 60), 'MI:SS')as hora_dia, coalesce(A.count,0) as recojo from(
select date_part('hour',created_at) as hora, count(*) from scharff_bot.logs 
where ((now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date) = (created_at)::date
and categoria ='recojo'
group by date_part('hour',created_at)
) A right join(
select * from generate_series(0,23) as h(hora)
)B on A.hora = B.hora) recojo ON estatus.hora_dia = recojo.hora_dia
LEFT JOIN
(select to_char(to_timestamp((B.hora) * 60), 'MI:SS')as hora_dia, coalesce(A.count,0) as tarifas from(
select date_part('hour',created_at) as hora, count(*) from scharff_bot.logs 
where ((now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date) = (created_at)::date
and categoria ='tarifas'
group by date_part('hour',created_at)
) A right join(
select * from generate_series(0,23) as h(hora)
)B on A.hora = B.hora) tarifas ON recojo.hora_dia = tarifas.hora_dia
LEFT JOIN
(select to_char(to_timestamp((B.hora) * 60), 'MI:SS')as hora_dia, coalesce(A.count,0) as contacto from(
select date_part('hour',created_at) as hora, count(*) from scharff_bot.logs 
where ((now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date) = (created_at)::date
and categoria ='contacto'
group by date_part('hour',created_at)
) A right join(
select * from generate_series(0,23) as h(hora)
)B on A.hora = B.hora) contacto ON contacto.hora_dia = tarifas.hora_dia
LEFT JOIN
(select to_char(to_timestamp((B.hora) * 60), 'MI:SS')as hora_dia, coalesce(A.count,0) as reclamos from(
select date_part('hour',created_at) as hora, count(*) from scharff_bot.logs 
where ((now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date) = (created_at)::date
and categoria ='reclamos'
group by date_part('hour',created_at)
) A right join(
select * from generate_series(0,23) as h(hora)
)B on A.hora = B.hora) reclamos ON reclamos.hora_dia = contacto.hora_dia
LEFT JOIN
(select to_char(to_timestamp((B.hora) * 60), 'MI:SS')as hora_dia, coalesce(A.count,0) as no_entiendo from(
select date_part('hour',created_at) as hora, count(*) from scharff_bot.logs 
where ((now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date) = (created_at)::date
and categoria ='no_entiendo'
group by date_part('hour',created_at)
) A right join(
select * from generate_series(0,23) as h(hora)
)B on A.hora = B.hora) no_entiendo ON no_entiendo.hora_dia = reclamos.hora_dia;

----Mensajes Categoria Line (SEMANA gmailTUAL)
create view scharff_bot.v_categoriaLine_Semana as 
SELECT estatus.fecha_semana, estatus.estatus, recojo.recojo, tarifas.tarifas, contacto.contacto,
reclamos.reclamos, no_entiendo.no_entiendo
FROM
(  
select B.fecha_semana, coalesce(A.count,0) as estatus 
from(
select (created_at)::date as fecha_semana, count(*) from scharff_bot.logs 
where date_trunc('week',created_at)=date_trunc('week', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)
and categoria ='estatus'
group by (created_at)::date
) A right join(
select ((date_trunc('week', now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date) + i) as fecha_semana 
from generate_Series(0,6) i
)B on A.fecha_semana = B.fecha_semana
) estatus
LEFT JOIN
(
select B.fecha_semana, coalesce(A.count,0) as recojo 
from(
select (created_at)::date as fecha_semana, count(*) from scharff_bot.logs 
where date_trunc('week',created_at)=date_trunc('week', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)
and categoria ='recojo'
group by (created_at)::date
) A right join(
select ((date_trunc('week', now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date) + i) as fecha_semana 
from generate_Series(0,6) i
)B on A.fecha_semana = B.fecha_semana
) recojo ON estatus.fecha_semana = recojo.fecha_semana
LEFT JOIN
(
select B.fecha_semana, coalesce(A.count,0) as tarifas 
from(
select (created_at)::date as fecha_semana, count(*) from scharff_bot.logs 
where date_trunc('week',created_at)=date_trunc('week', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)
and categoria ='tarifas'
group by (created_at)::date
) A right join(
select ((date_trunc('week', now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date) + i) as fecha_semana 
from generate_Series(0,6) i
)B on A.fecha_semana = B.fecha_semana
) tarifas ON recojo.fecha_semana = tarifas.fecha_semana
LEFT JOIN
(
select B.fecha_semana, coalesce(A.count,0) as contacto 
from(
select (created_at)::date as fecha_semana, count(*) from scharff_bot.logs 
where date_trunc('week',created_at)=date_trunc('week', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)
and categoria ='contacto'
group by (created_at)::date
) A right join(
select ((date_trunc('week', now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date) + i) as fecha_semana 
from generate_Series(0,6) i
)B on A.fecha_semana = B.fecha_semana
) contacto ON contacto.fecha_semana = tarifas.fecha_semana
LEFT JOIN
(
select B.fecha_semana, coalesce(A.count,0) as reclamos 
from(
select (created_at)::date as fecha_semana, count(*) from scharff_bot.logs 
where date_trunc('week',created_at)=date_trunc('week', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)
and categoria ='reclamos'
group by (created_at)::date
) A right join(
select ((date_trunc('week', now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date) + i) as fecha_semana 
from generate_Series(0,6) i
)B on A.fecha_semana = B.fecha_semana
) reclamos ON reclamos.fecha_semana = contacto.fecha_semana
LEFT JOIN
(
select B.fecha_semana, coalesce(A.count,0) as no_entiendo 
from(
select (created_at)::date as fecha_semana, count(*) from scharff_bot.logs 
where date_trunc('week',created_at)=date_trunc('week', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)
and categoria ='no_entiendo'
group by (created_at)::date
) A right join(
select ((date_trunc('week', now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date) + i) as fecha_semana 
from generate_Series(0,6) i
)B on A.fecha_semana = B.fecha_semana
) no_entiendo ON no_entiendo.fecha_semana = reclamos.fecha_semana;

----Mensajes Categoria Line (MES gmailTUAL)
create view scharff_bot.v_categoriaLine_Mes as 
SELECT estatus.fecha_mes, estatus.estatus, recojo.recojo, tarifas.tarifas, contacto.contacto,
reclamos.reclamos, no_entiendo.no_entiendo
FROM
(  
select B.fecha_mes, coalesce(A.count,0) as estatus 
from(
select (created_at)::date as fecha_mes, count(*) from scharff_bot.logs 
where date_trunc('month',created_at)=date_trunc('month', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)
and categoria ='estatus'
group by (created_at)::date
) A right join(
select date::date as fecha_mes
from generate_series(
  date_trunc('month', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)::date,
  (date_trunc('month', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date) + interval '1 month' - interval '1 day')::date,
  '1 day'::interval
) date
)B on A.fecha_mes = B.fecha_mes
) estatus
LEFT JOIN
(
select B.fecha_mes, coalesce(A.count,0) as recojo 
from(
select (created_at)::date as fecha_mes, count(*) from scharff_bot.logs 
where date_trunc('month',created_at)=date_trunc('month', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)
and categoria ='recojo'
group by (created_at)::date
) A right join(
select date::date as fecha_mes
from generate_series(
  date_trunc('month', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)::date,
  (date_trunc('month', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date) + interval '1 month' - interval '1 day')::date,
  '1 day'::interval
) date
)B on A.fecha_mes = B.fecha_mes
) recojo ON estatus.fecha_mes = recojo.fecha_mes
LEFT JOIN
(
select B.fecha_mes, coalesce(A.count,0) as tarifas 
from(
select (created_at)::date as fecha_mes, count(*) from scharff_bot.logs 
where date_trunc('month',created_at)=date_trunc('month', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)
and categoria ='tarifas'
group by (created_at)::date
) A right join(
select date::date as fecha_mes
from generate_series(
  date_trunc('month', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)::date,
  (date_trunc('month', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date) + interval '1 month' - interval '1 day')::date,
  '1 day'::interval
) date
)B on A.fecha_mes = B.fecha_mes
) tarifas ON recojo.fecha_mes = tarifas.fecha_mes
LEFT JOIN
(
select B.fecha_mes, coalesce(A.count,0) as contacto 
from(
select (created_at)::date as fecha_mes, count(*) from scharff_bot.logs 
where date_trunc('month',created_at)=date_trunc('month', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)
and categoria ='contacto'
group by (created_at)::date
) A right join(
select date::date as fecha_mes
from generate_series(
  date_trunc('month', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)::date,
  (date_trunc('month', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date) + interval '1 month' - interval '1 day')::date,
  '1 day'::interval
) date
)B on A.fecha_mes = B.fecha_mes
) contacto ON contacto.fecha_mes = tarifas.fecha_mes
LEFT JOIN
(
select B.fecha_mes, coalesce(A.count,0) as reclamos 
from(
select (created_at)::date as fecha_mes, count(*) from scharff_bot.logs 
where date_trunc('month',created_at)=date_trunc('month', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)
and categoria ='reclamos'
group by (created_at)::date
) A right join(
select date::date as fecha_mes
from generate_series(
  date_trunc('month', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)::date,
  (date_trunc('month', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date) + interval '1 month' - interval '1 day')::date,
  '1 day'::interval
) date
)B on A.fecha_mes = B.fecha_mes
) reclamos ON reclamos.fecha_mes = contacto.fecha_mes
LEFT JOIN
(
select B.fecha_mes, coalesce(A.count,0) as no_entiendo 
from(
select (created_at)::date as fecha_mes, count(*) from scharff_bot.logs 
where date_trunc('month',created_at)=date_trunc('month', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)
and categoria ='no_entiendo'
group by (created_at)::date
) A right join(
select date::date as fecha_mes
from generate_series(
  date_trunc('month', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)::date,
  (date_trunc('month', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date) + interval '1 month' - interval '1 day')::date,
  '1 day'::interval
) date
)B on A.fecha_mes = B.fecha_mes
) no_entiendo ON no_entiendo.fecha_mes = reclamos.fecha_mes;

-----Mensajes categoria linea (RANGO)


--select * from scharff_bot.categoriaLine_Rango('2018-01-31','2018-02-22')

CREATE OR REPLACE FUNCTION scharff_bot.categoriaLine_Rango(start_date date, end_date date)
  RETURNS TABLE (
  fecha_mes date,
  estatus bigint,
  recojo bigint,
  tarifas bigint,
  contacto bigint,
  reclamos bigint,
  no_entiendo bigint) as
$BODY$
BEGIN 
   RETURN QUERY
 SELECT estatus.fecha_mes, estatus.estatus, recojo.recojo, tarifas.tarifas, contacto.contacto,
reclamos.reclamos, no_entiendo.no_entiendo
FROM
(  
select B.fecha_mes, coalesce(A.count,0) as estatus 
from(
select (created_at)::date as fecha_mes, count(*) from scharff_bot.logs 
where created_at >= start_date and created_at <= end_date
and categoria ='estatus'
group by (created_at)::date
) A right join(
select date::date as fecha_mes
from generate_series(
  start_date,
  end_date,
  '1 day'::interval
) date
)B on A.fecha_mes = B.fecha_mes
) estatus
LEFT JOIN
(
select B.fecha_mes, coalesce(A.count,0) as recojo 
from(
select (created_at)::date as fecha_mes, count(*) from scharff_bot.logs 
where created_at >= start_date and created_at <= end_date
and categoria ='recojo'
group by (created_at)::date
) A right join(
select date::date as fecha_mes
from generate_series(
  start_date,
  end_date,
  '1 day'::interval
) date
)B on A.fecha_mes = B.fecha_mes
) recojo ON estatus.fecha_mes = recojo.fecha_mes
LEFT JOIN
(
select B.fecha_mes, coalesce(A.count,0) as tarifas 
from(
select (created_at)::date as fecha_mes, count(*) from scharff_bot.logs 
where created_at >= start_date and created_at <= end_date
and categoria ='tarifas'
group by (created_at)::date
) A right join(
select date::date as fecha_mes
from generate_series(
  start_date,
  end_date,
  '1 day'::interval
) date
)B on A.fecha_mes = B.fecha_mes
) tarifas ON recojo.fecha_mes = tarifas.fecha_mes
LEFT JOIN
(
select B.fecha_mes, coalesce(A.count,0) as contacto 
from(
select (created_at)::date as fecha_mes, count(*) from scharff_bot.logs 
where created_at >= start_date and created_at <= end_date
and categoria ='contacto'
group by (created_at)::date
) A right join(
select date::date as fecha_mes
from generate_series(
  start_date,
  end_date,
  '1 day'::interval
) date
)B on A.fecha_mes = B.fecha_mes
) contacto ON contacto.fecha_mes = tarifas.fecha_mes
LEFT JOIN
(
select B.fecha_mes, coalesce(A.count,0) as reclamos 
from(
select (created_at)::date as fecha_mes, count(*) from scharff_bot.logs 
where created_at >= start_date and created_at <= end_date
and categoria ='reclamos'
group by (created_at)::date
) A right join(
select date::date as fecha_mes
from generate_series(
  start_date,
  end_date,
  '1 day'::interval
) date
)B on A.fecha_mes = B.fecha_mes
) reclamos ON reclamos.fecha_mes = contacto.fecha_mes
LEFT JOIN
(
select B.fecha_mes, coalesce(A.count,0) as no_entiendo 
from(
select (created_at)::date as fecha_mes, count(*) from scharff_bot.logs 
where created_at >= start_date and created_at <= end_date
and categoria ='no_entiendo'
group by (created_at)::date
) A right join(
select date::date as fecha_mes
from generate_series(
  start_date,
  end_date,
  '1 day'::interval
) date
)B on A.fecha_mes = B.fecha_mes
) no_entiendo ON no_entiendo.fecha_mes = reclamos.fecha_mes;

    
END;
$BODY$ 
LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------
---Mensajes y Conversaciones Line (DIA gmailTUAL)
create view scharff_bot.v_mensajesConversacionesLine_Dia as 
select MENSAJES.hora_dia, MENSAJES.mensajes,CONVERSACIONES.conversaciones from
(select B.hora as hora_dia, coalesce(A.count,0) as mensajes from(
select date_part('hour',created_at) as hora, count(*) from scharff_bot.logs 
where ((now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date) = (created_at)::date
group by date_part('hour',created_at)
    ) A right join(
    select * from generate_series(0,23) as h(hora)
)B on A.hora = B.hora) MENSAJES
LEFT JOIN
(select B.hora as hora_dia, coalesce(A.count,0) as conversaciones from(
select X.hora, count (X.*) from 
    (select distinct conversation_id, date_part('hour',created_at) as hora from scharff_bot.logs 
    where ((now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date) = (created_at)::date
    group by conversation_id,date_part('hour',created_at)) X group by X.hora
    ) A right join(
    select * from generate_series(0,23) as h(hora)
)B on A.hora = B.hora) CONVERSACIONES ON MENSAJES.hora_dia = CONVERSACIONES.hora_dia;


---Mensajes y Conversaciones Line (SEMANA gmailTUAL)
create view scharff_bot.v_mensajesConversacionesLine_Semana as 
select MENSAJES.fecha_semana, MENSAJES.mensajes,CONVERSACIONES.conversaciones from
(
    select B.fecha_semana,coalesce(A.count , 0) as mensajes from (select (created_at)::date AS fecha,count (*) from scharff_bot.logs 
    where date_trunc('week',created_at)=date_trunc('week', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)
    group by (created_at)::date) A 
    right join (
        select ((date_trunc('week', now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date) + i) as fecha_semana 
    from generate_Series(0,6) i) B ON A.fecha = B.fecha_semana)MENSAJES
    LEFT JOIN
(
    select B.fecha_semana,coalesce(A.count , 0) as conversaciones from(
    select X.fecha, count (X.*) from 
    (select conversation_id, (created_at)::date AS fecha from scharff_bot.logs 
    where date_trunc('week',created_at)=date_trunc('week', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)
    group by conversation_id,(created_at)::date) X group by X.fecha) A
    right join (
        select ((date_trunc('week', now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date) + i) as fecha_semana 
    from generate_Series(0,6) i) B ON A.fecha = B.fecha_semana) CONVERSACIONES 
    ON MENSAJES.fecha_semana= CONVERSACIONES.fecha_semana;


---Mensajes y Conversaciones Line (MES gmailTUAL)

create view scharff_bot.v_mensajesConversacionesLine_Mes as 
select MENSAJES.fecha_mes, MENSAJES.mensajes,CONVERSACIONES.conversaciones from
(
    select B.fecha_mes,coalesce(A.count , 0) as mensajes from (select (created_at)::date AS fecha,count (*) from scharff_bot.logs 
    where date_trunc('month',created_at)=date_trunc('month', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)
    group by (created_at)::date) A 
    right join (
        select date::date as fecha_mes
from generate_series(
  date_trunc('month', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)::date,
  (date_trunc('month', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date) + interval '1 month' - interval '1 day')::date,
  '1 day'::interval
) date) B ON A.fecha = B.fecha_mes)MENSAJES
    LEFT JOIN
(
    select B.fecha_mes,coalesce(A.count , 0) as conversaciones from(
    select X.fecha, count (X.*) from 
    (select conversation_id, (created_at)::date AS fecha from scharff_bot.logs 
    where date_trunc('month',created_at)=date_trunc('month', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)
    group by conversation_id,(created_at)::date) X group by X.fecha) A
    right join (
        select date::date as fecha_mes
from generate_series(
  date_trunc('month', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)::date,
  (date_trunc('month', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date) + interval '1 month' - interval '1 day')::date,
  '1 day'::interval
) date) B ON A.fecha = B.fecha_mes) CONVERSACIONES 
    ON MENSAJES.fecha_mes= CONVERSACIONES.fecha_mes;

-----Mensajes y Conversaciones LINE (RANGO)

--select * from mensajesConversacionesLine_Rango('2018-01-14', '2018-02-01');
 

------------------------------------------------------------------------------------------------------------------------
--Contadores (DIA,SEMANA, MES gmailTUAL)

create view scharff_bot.v_contadoresGeneral as   
select X.tiempo, X.count as mensajes, Y.count as conversaciones,
CASE WHEN Y.count = 0 THEN Y.count
   ELSE round(cast((cast(X.count as float)/cast(Y.count as float)) as numeric),2) 
   END as promedio,
Z.count as usuarios
from
(select 'dia' as tiempo, count(*) from scharff_bot.logs
where ((now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date) = (created_at)::date
UNION
select 'semana' as tiempo, count(*) from scharff_bot.logs
where date_trunc('week',created_at)=date_trunc('week', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)
UNION
select 'mes' as tiempo, count(*) from scharff_bot.logs
where date_trunc('month',created_at)=date_trunc('month', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date))X
JOIN
(select 'dia' as tiempo, count (A.*) from (select conversation_id from scharff_bot.logs 
where ((now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date) = (created_at)::date
group by conversation_id) A
UNION
select 'semana' as tiempo, count (B.*) from (select conversation_id from scharff_bot.logs 
where date_trunc('week',created_at)=date_trunc('week', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)
group by conversation_id) B
UNION
select 'mes' as tiempo, count (C.*) from (select conversation_id from scharff_bot.logs 
where date_trunc('month',created_at)=date_trunc('month', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)
group by conversation_id) C)Y on X.tiempo = Y.tiempo
JOIN
(select 'dia' as tiempo, count (A.*) from (select codigo_fb from scharff_bot.logs 
where ((now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date) = (created_at)::date
group by codigo_fb) A
UNION
select 'semana' as tiempo, count (B.*) from (select codigo_fb from scharff_bot.logs 
where date_trunc('week',created_at)=date_trunc('week', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)
group by codigo_fb) B
UNION
select 'mes' as tiempo, count (C.*) from (select codigo_fb from scharff_bot.logs 
where date_trunc('month',created_at)=date_trunc('month', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)
group by codigo_fb) C) Z on Z.tiempo =X.tiempo;

--Contadores RANGO

--select * from scharff_bot.contadoresGeneral_Rango('2018-01-14', '2018-02-01');


CREATE OR REPLACE FUNCTION scharff_bot.contadoresGeneral_Rango(start_date date, end_date date)
  RETURNS TABLE (
  tiempo text,
  mensajes bigint,
  conversaciones bigint,
  promedio numeric,
  usuarios bigint) as
$BODY$
BEGIN 
   RETURN QUERY
   
   select X.tiempo, X.count as mensajes, Y.count as conversaciones,
   CASE WHEN Y.count = 0 THEN Y.count
   ELSE round(cast((cast(X.count as float)/cast(Y.count as float)) as numeric),2) 
   END as promedio,
    Z.count as usuarios
    from
    (select 'rango'::text as tiempo, count(*) from scharff_bot.logs
    where created_at >= start_date and created_at <= end_date
    )X
    LEFT JOIN
    (select 'rango'::text as tiempo, count (A.*) from (select conversation_id from scharff_bot.logs 
    where created_at >= start_date and created_at <= end_date
    group by conversation_id) A
    )Y on X.tiempo = Y.tiempo
    JOIN
    (select 'rango'::text as tiempo, count (A.*) from (select codigo_fb from scharff_bot.logs 
    where created_at >= start_date and created_at <= end_date
    group by codigo_fb) A) Z on Z.tiempo =X.tiempo;
    
END;
$BODY$ 
LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------

-----SubCategoriascanal (DIA gmailTUAL)
create view scharff_bot.v_subCategoriascanal_Dia as 
SELECT S.nombre as categoria, A.nombre as subcategoria,A.descripcion as nombre, coalesce(B.facebook,0) as facebook, coalesce(C.gmail,0) as gmail from
(select id_categoria,nombre, descripcion from scharff_bot.subcategoria) A
LEFT JOIN
(select id, nombre from scharff_bot.categoria) S on A.id_categoria = S.id
LEFT JOIN 
(select subcategoria, count(*) as facebook from scharff_bot.logs where canal ='facebook' and subcategoria!= '' and subcategoria is not null
and (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date = (created_at)::date 
group by subcategoria) B ON A.nombre = B.subcategoria
LEFT JOIN
(select subcategoria, count(*)as gmail from scharff_bot.logs where canal ='gmail' and subcategoria!= '' and subcategoria is not null
and (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date = (created_at)::date 
group by subcategoria) C ON A.nombre = C.subcategoria
order by categoria,subcategoria;

-----SubCategoriascanal (SEMANA gmailTUAL)
create view scharff_bot.v_subCategoriascanal_Semana as 
SELECT S.nombre as categoria, A.nombre as subcategoria,A.descripcion as nombre, coalesce(B.facebook,0) as facebook, coalesce(C.gmail,0) as gmail from
(select id_categoria,nombre, descripcion from scharff_bot.subcategoria) A
LEFT JOIN
(select id, nombre from scharff_bot.categoria) S on A.id_categoria = S.id
LEFT JOIN 
(select subcategoria, count(*) as facebook from scharff_bot.logs where canal ='facebook' and subcategoria!= '' and subcategoria is not null
and  date_trunc('week',created_at)=date_trunc('week', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)
group by subcategoria) B ON A.nombre = B.subcategoria
LEFT JOIN
(select subcategoria, count(*)as gmail from scharff_bot.logs where canal ='gmail' and subcategoria!= '' and subcategoria is not null
and  date_trunc('week',created_at)=date_trunc('week', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)
group by subcategoria) C ON A.nombre = C.subcategoria
order by categoria,subcategoria;

-----SubCategoriascanal (MES gmailTUAL)
create view scharff_bot.v_subCategoriascanal_Mes as 
SELECT S.nombre as categoria, A.nombre as subcategoria,A.descripcion as nombre, coalesce(B.facebook,0) as facebook, coalesce(C.gmail,0) as gmail from
(select id_categoria,nombre, descripcion from scharff_bot.subcategoria) A
LEFT JOIN
(select id, nombre from scharff_bot.categoria) S on A.id_categoria = S.id
LEFT JOIN 
(select subcategoria, count(*) as facebook from scharff_bot.logs where canal ='facebook' and subcategoria!= '' and subcategoria is not null
and  date_trunc('month',created_at)=date_trunc('month', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)
group by subcategoria) B ON A.nombre = B.subcategoria
LEFT JOIN
(select subcategoria, count(*)as gmail from scharff_bot.logs where canal ='gmail' and subcategoria!= '' and subcategoria is not null
and  date_trunc('month',created_at)=date_trunc('month', (now()::TIMESTAMP AT TIME ZONE 'UTC-5:00')::date)
group by subcategoria) C ON A.nombre = C.subcategoria
order by categoria,subcategoria;

---SubCategoriascanal (RANGO)

--select * from scharff_bot.subCategoriascanal_Rango('2018-01-11','2018-02-22')

CREATE OR REPLACE FUNCTION scharff_bot.subCategoriascanal_Rango(start_date date, end_date date)
  RETURNS TABLE (
  categoria varchar,
  subcategoria varchar,
  nombre varchar,
  facebook int,
  gmail int) as
$BODY$
BEGIN 
RETURN QUERY

SELECT S.nombre as categoria, A.nombre as subcategoria,A.descripcion as nombre, coalesce(B.facebook,0)::int as facebook, coalesce(C.gmail,0)::int as gmail from
(select L.id_categoria, L.nombre, L.descripcion from scharff_bot.subcategoria L) A
LEFT JOIN
(select X.id, X.nombre from scharff_bot.categoria X) S on A.id_categoria = S.id
LEFT JOIN 
(select L.subcategoria, count(L.*) as facebook from scharff_bot.logs L where L.canal ='facebook' and L.subcategoria!= '' and L.subcategoria is not null
and  L.created_at >= start_date and L.created_at <= end_date
group by L.subcategoria) B ON A.nombre = B.subcategoria
LEFT JOIN
(select L.subcategoria, count(L.*) as gmail from scharff_bot.logs L where L.canal ='gmail' and L.subcategoria!= '' and L.subcategoria is not null
and  L.created_at >= start_date and L.created_at <= end_date
group by L.subcategoria) C ON A.nombre = C.subcategoria
order by categoria,subcategoria;
    
END;
$BODY$ 
LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------

